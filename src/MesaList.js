import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';

class MesaList extends Component {
	emptyItem = {
		mesaId: '',
		salaId: 3,
		numero: ''
	};

	constructor(props) {
		super(props);
		this.state = {mesas: [], isLoading: true, item: this.emptyItem};
		this.remove = this.remove.bind(this);

		// metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		this.setState({isLoading: true});
		fetch('/Mesa/findBySalaId/3')
			.then(response => response.json())
			.then(data => this.setState({mesas: data, isLoading: false}));
	}

	handleChange(event) {
		const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
	}

	toggle() {
      this.setState({modal: !this.state.modal});
	}
	
	async guardarDatos(event) {
		event.preventDefault();
		const {item} = this.state;

		await fetch('/Mesa/insertOrUpdate', {
			method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
		});

		this.setState({item: this.emptyItem});
      this.componentDidMount();
      this.toggle();
	}

	async remove(id) {
		await fetch(`/Mesa/deleteById/${id}`,{
			method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
		}).then(() => {
			let updatedMesas = [...this.state.mesas].filter(i => i.mesaId !== id);
			this.setState({mesas: updatedMesas});
		});
	}

	async editar(id) {
		const mesa = await(await fetch(`/Mesa/findById/${id}`)).json();
		this.setState({item: mesa});
		this.toggle();
	}

	render() {
		const {mesas, isLoading} = this.state;
		const {item} = this.state;
		const titulo = <h3>{item.mesaId ? 'Editar Mesa' : 'Adicionar Mesa'}</h3>;

		if(isLoading) {
			return <p>Cargando...</p>;
		}

		const mesaList = mesas.map(mesa => {
			return <tr key={mesa.mesaId}>
				<td>{mesa.numero}</td>
				<td> 
               <ButtonGroup>
                  <Button size="sm" color="primary" onClick={() => this.editar(mesa.mesaId)}>Editar</Button>
                  <Button size="sm" color="danger" onClick={() => this.remove(mesa.mesaId)}>Borrar</Button>
               </ButtonGroup>
            </td>
			</tr>
		});

		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<Modal isOpen={this.state.modal}>
						<form onSubmit={this.guardarDatos}>
							<ModalHeader>{titulo}</ModalHeader>
							<ModalBody>
								<div className="row">
									<div className="form-group col-md-8">
                              <label>Número:</label>
                              <input type="text" name="numero" value={item.numero} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
								</div>
							</ModalBody>
							<ModalFooter>
								<input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
							</ModalFooter>
						</form>
					</Modal>
					<h3>Listado de Mesas</h3>
					<Button color="success" onClick={this.toggle}>Adicionar</Button>
					<Table className="mt-4">
						<thead>
							<tr>
								<th>Número</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							{mesaList}
						</tbody>
					</Table>
				</Container>
			</div>
		)
	}
}

export default MesaList;