import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import { server } from './servercontext';

class TipoGastoList extends Component {
   emptyItem = {
      tipoGastoId: '',
      descripcion: ''
   };

   constructor(props) {
      super(props);
      this.state = {tipogastos: [], isLoading: true, item: this.emptyItem};
      this.remove = this.remove.bind(this);

      // metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/tipoGasto/findAll`)
         .then(res => {
            this.setState({tipogastos: res.data, isLoading: false});
         });
      /*fetch('/tipoGasto/findAll')
         .then(response => response.json())
         .then(data => this.setState({tipogastos: data, isLoading: false}));*/
   }

   handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }

   toggle() {
      this.setState({modal: !this.state.modal});
   }

   async guardarDatos(event) {
      event.preventDefault();
      const {item} = this.state;

      await axios.post(`${server.ip}/tipoGasto/insertOrUpdate`, item)
         .then(res => {

         });
      /*fetch('/tipoGasto/insertOrUpdate', {
         method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
      });*/

      this.setState({item: this.emptyItem});
      this.componentDidMount();
      this.toggle();
   }

   async remove(id) {
      await axios.get(`${server.ip}/tipoGasto/deleteById/${id}`)
         .then(res =>{
            let updatedTipogastos = [...this.state.tipogastos].filter(i => i.tipoGastoId !== id);
            this.setState({tipogastos: updatedTipogastos});   
         })
      /*fetch(`/tipoGasto/deleteById/${id}`, {
         method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
      }).then(() =>{
         let updatedTipogastos = [...this.state.tipogastos].filter(i => i.tipoGastoId !== id);
         this.setState({tipogastos: updatedTipogastos});
      });*/
   }

   async editar(id) {
      await axios.get(`${server.ip}/tipoGasto/findById/${id}`)
         .then(res => {
            this.setState({item: res.data});
            this.toggle();
         });
      /*const tipogasto = await(await fetch(`/tipoGasto/findById/${id}`)).json();
      this.setState({item: tipogasto});
      this.toggle();*/
   }
   render() {
      const {tipogastos, isLoading} = this.state;
      const {item} = this.state;
      const titulo = <h3>{item.tipoGastoId ? 'Editar Tipo Gasto' : 'Adicionar Tipo Gasto'}</h3>;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const tipogastoList = tipogastos.map(tgas => {
         return <tr key={tgas.tipoGastoId}>
            <td>{tgas.descripcion}</td>
            <td> 
               <ButtonGroup>
                  <Button size="sm" color="primary" onClick={() => this.editar(tgas.tipoGastoId)}>Editar</Button>
                  <Button size="sm" color="danger" onClick={() => this.remove(tgas.tipoGastoId)}>Borrar</Button>
               </ButtonGroup>
            </td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarDatos}>
                     <ModalHeader>{titulo}</ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Descripción:</label>
                              <input type="text" name="descripcion" value={item.descripcion} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Tipos de Gastos</h3>
               <Button color="success" onClick={this.toggle}>Adicionar</Button>
               <Table>
                  <thead>
                     <tr>
                        <th>Descripción</th>
                        <th>Acciones</th>
                     </tr>
                  </thead>
                  <tbody>
                     {tipogastoList}
                  </tbody>
               </Table>
            </Container>
         </div>
      );
   }
}

export default TipoGastoList;