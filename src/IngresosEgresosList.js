import React, { Component } from 'react';
import { Container, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import DatePicker from "react-datepicker";
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-confirm-alert/src/react-confirm-alert.css';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { server } from './servercontext';

class IngresosEgresosList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         ingresos: [], 
         isLoading: false,
         fechaInicio: new Date(),
         fechaFin: new Date(),
         fechaComanda: new Date(),
         comandas: [],
         detalles: [],
         damas: [],
         totalIngreso: 0,
         totalEgreso: 0,
         modalDama: false,
         totalComisionDama: 0,
         totalComision: 0
      }
      this.handleChangeFechaInicio = this.handleChangeFechaInicio.bind(this);
      this.handleChangeFechaFin = this.handleChangeFechaFin.bind(this);
      this.handleChangeFechaComandas = this.handleChangeFechaComandas.bind(this);
      this.mostrarMovimientos = this.mostrarMovimientos.bind(this);
      this.imprimir = this.imprimir.bind(this);
      this.mostrarComandas = this.mostrarComandas.bind(this);
      this.mostrarDetalleComanda = this.mostrarDetalleComanda.bind(this);
      this.cerrarDetalle = this.cerrarDetalle.bind(this);
      this.abrirModalChica = this.abrirModalChica.bind(this);
      this.cerrarModalChica = this.cerrarModalChica.bind(this);
   }

   handleChangeFechaInicio(date1){
      this.setState({fechaInicio: date1});
   }
   handleChangeFechaFin(date1){
      this.setState({fechaFin: date1});
   }
   handleChangeFechaComandas(date1){
      this.setState({fechaComanda: date1});
   }
   cerrarDetalle(){
      this.setState({modalDetalle: !this.state.modalDetalle});
   }
   cerrarModalChica(){
      this.setState({modalDama: false});
   }
   async abrirModalChica(damaId){
      console.log(damaId);
      
      const parametro = {
         id: damaId,
         fechaInicio: this.state.fechaInicio,
         fechaFin: this.state.fechaFin
      };

      await axios.post(`${server.ip}/PedidoDama/findByDamaIdFecha`, parametro)
         .then(res => {
            console.log(res.data);
            
            this.setState({damas: res.data});
            var total = 0;
            for(var i = 0; i < this.state.damas.length; i++) {
               total += this.state.damas[i].comision;
            }
            this.setState({totalComisionDama: total});
         });
      
      this.setState({modalDama: true});
   }
   mostrarComandas(){
      
      const parametros = {
         fechaInicio: this.state.fechaComanda
      };
      this.setState({isLoading: true});
      axios.post(`${server.ip}/ComandasMeseros/obtenerTotalesComandasByFecha`, parametros)
         .then(res => {
            this.setState({comandas: res.data, isLoading: false});
         });
   }
   mostrarTipoPago(tipo){
      if(tipo === 1011){
         return "AL CONTADO";
      }
      if(tipo === 1012){
         return "A CREDITO";
      }
   }

   mostrarEstadoPedido(estado){
      if(estado === 1003){
         return "SOLICITADO";
      }
      if(estado === 1004){
         return "ATENDIDO";
      }
      if(estado === 1005){
         return "CANCELADO";
      }
   }
   imprimir(){
      //console.log("print");
      var content = document.getElementById("printIngresos").innerHTML;
      var mywindow = window.open('', 'height=600,width=800');
      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      mywindow.print();
      mywindow.close();
      return true;
   }
   async mostrarDetalleComanda(usuarioId){
      this.setState({modalDetalle: !this.state.modalDetalle, isLoading: true});
      const parametros = {
         id: usuarioId,
         fechaInicio: this.state.fechaComanda
      };
      await axios.post(`${server.ip}/DetalleComandas/findByUsuarioIdFecha`, parametros)
         .then(res => {
            console.log(res.data);  
            this.setState({detalles: res.data, isLoading: false});
         })
      
   }
   mostrarMovimientos(event){
      const parametros = {
         id: 1004,
         fechaInicio: this.state.fechaInicio,
         fechaFin: this.state.fechaFin
      };
      //console.log(parametros);
      this.setState({isLoading: true});
      axios.post(`${server.ip}/Pedido/findByEstadoPedidoIdFechas`, parametros)
         .then(res => {
            console.log(res.data);
            this.setState({ingresos: res.data, isLoading: false});
            var totalIngreso1 = 0;
            var totalComision1 = 0;
            //var totalEgresos1 = 0;
            for(var i = 0; i < this.state.ingresos.length; i++){
               totalIngreso1 += this.state.ingresos[i].montoTotal;
               for(var j = 0; j < this.state.ingresos[i].detalle.length; j++){
                  for(var k = 0; k < this.state.ingresos[i].detalle[j].pedidosDama.length; k++){
                     totalComision1 += this.state.ingresos[i].detalle[j].pedidosDama[k].comision;
                  }
               }
            }
            this.setState({totalIngreso: totalIngreso1, totalComision: totalComision1});
         });
   }

   render() {
      const {ingresos, isLoading, totalIngreso, comandas, detalles, damas, totalComisionDama, totalComision} = this.state;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const movimientoList = ingresos.map(ped => {
         return <tr key={ped.pedidoId}>
            <td>
               <Moment format="DD/MM/YYYY">
                  {ped.fechaRegistro}
               </Moment>
            </td>
            <td>{ped.numeroPedido}</td>
            <td>{ped.montoTotal}</td>
            <td>
               <table>
                  {ped.detalle.map(d => <tr key={d.detallePedidoId}>
                  <td width="20">{d.cantidad}</td>
                  <td width="200">{d.nombreSubproducto}</td>
                  <td width="50" align="right">{d.precioUnitario}</td>
                  <td>
                     <ul>
                        {d.pedidosDama.map(da => <li key={da.damaId}><Link to='#' onClick={() => this.abrirModalChica(da.damaId)}>{da.nombreDama}</Link> </li>)}
                     </ul> 
                  </td> 
                  </tr>)}
               </table>
            </td>
         </tr>
      });

      const pedidoList = detalles.map(det => {
         return <tr>
            <td>{det.nroPedido}</td>
            <td>
               <Moment format="DD/MM/YYYYY HH:MM">
                  {det.fechaRegistro}
               </Moment>
            </td>
            <td>{this.mostrarEstadoPedido(det.estadoPedidoId)}</td>
            <td>{this.mostrarTipoPago(det.tipoPagoId)}</td>
            <td>
               <ul>
                  {det.datosPedido.map(d => <li key={d.detallePedidoId}>{d.cantidad} - {d.nombreSubproducto}
                  {d.damasConcatenadas}
                  </li>)}
               </ul>
            </td>
         </tr>
      });

      const comandaList = comandas.map(com => {
         return <tr key={com.usuarioId}>
            <td>{com.username}</td>
            <td align="right" width="50">{com.total}</td>
            <td align="right" width="50" >{com.total * 0.05}</td>
            <td><Button color="success" onClick={() => this.mostrarDetalleComanda(com.usuarioId)}> Ver Detalle</Button></td>
         </tr>
      });

      const damaList = damas.map(da => {
         return <tr key={da.pedidoDamaId}>
            <td>{da.numeroPedido}</td>
            <td>
               <Moment format="DD/MM/YYYY HH:MM">
                  {da.fechaRegistro}
               </Moment>
            </td>
            <td>{da.cantidad}</td>
            <td>{da.nombreProducto}</td>
            <td>{da.nombreDama}</td>
            <td align="right">{da.comision}</td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Tabs>
                  <TabList>
                     <Tab>
                        Ingresos
                     </Tab>
                     <Tab>
                        Pedidos por Garzon
                     </Tab>
                  </TabList>
                  <TabPanel>
                     <Modal isOpen={this.state.modalDama} size="lg">
                        <ModalHeader>Detalle Pedidos</ModalHeader>
                        <ModalBody>
                           <Table className="mt-4">
                              <thead>
                                 <tr>
                                    <th>Número Pedido</th>
                                    <th>Fecha y Hora</th>
                                    <th>Cantidad</th>
                                    <th>Producto</th>
                                    <th>Nombre</th>
                                    <th>Comisión</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 {damaList}
                                 <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>Total:</b></td>
                                    <td align="right"><b>{totalComisionDama}</b></td>
                                 </tr>
                              </tbody>
                           </Table>
                        </ModalBody>
                        <ModalFooter>
                           <Button color="primary" className="btn btn-primary" 
                              onClick={this.cerrarModalChica} >Cerrar</Button>
                        </ModalFooter>
                     </Modal>
                     
                     <div id="printIngresos">
                        <h3>Ingresos de Bebidas por Periodo</h3>
                        <br/>
                        <form className="form-inline">
                           <div className="form-group">
                              <label>Inicio: </label>
                              <DatePicker
                                 selected={this.state.fechaInicio}
                                 onChange={this.handleChangeFechaInicio}
                                 dateFormat="dd/MM/yyyy"
                                 className="form-control col-md-8"
                                 />
                           </div>
                           <div className="form-group">
                              <label> Fin: </label>
                              <DatePicker
                                 selected={this.state.fechaFin}
                                 onChange={this.handleChangeFechaFin}
                                 dateFormat="dd/MM/yyyy"
                                 className="form-control col-md-8"
                                 />
                           </div>
                           <button type="button" class="btn btn-primary" onClick={this.mostrarMovimientos}>Mostrar</button>
                           <b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Ingresos: {totalIngreso}</b>
                        </form>
                        
                        <Table className="mt-4">
                           <thead>
                              <tr>
                                 <th>Fecha</th>
                                 <th>Nro. Pedido</th>
                                 <th>Monto Total</th>
                                 <th>Detalle
                                    <table>
                                       <tr>
                                          <td width="15">Cant.</td>
                                          <td width="155">SubProducto</td>
                                          <td width="50" >Precio Unitario</td>
                                          <td></td> 
                                       </tr>
                                    </table>
                                 </th>
                              </tr>
                           </thead>
                           <tbody>
                              {movimientoList}
                              <tr>
                                 <td></td>
                                 <td><b>Total Ingresos (Bs):</b></td>
                                 <td><b>{totalIngreso}</b> </td>
                                 <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total comisiones (Bs): {totalComision}</b> </td>
                              </tr>
                           </tbody>
                        </Table>
                     </div>
                  </TabPanel>
                  <TabPanel>
                     <Modal isOpen={this.state.modalDetalle} size="lg">
                        <ModalHeader>Detalle Pedidos</ModalHeader>
                        <ModalBody>
                           <Table className="mt-4">
                              <thead>
                                 <tr>
                                    <th>Numero Pedido</th>
                                    <th>Fecha y Hora Pedido</th>
                                    <th>Estado Pedido</th>
                                    <th>Pago</th>
                                    <th>Detalle </th>
                                 </tr>
                              </thead>
                              <tbody>
                                 {pedidoList}
                              </tbody>
                           </Table>
                        </ModalBody>
                        <ModalFooter>
                           <Button color="primary" className="btn btn-primary" 
                              onClick={this.cerrarDetalle} >Cerrar</Button>
                        </ModalFooter>
                     </Modal>
                     <form className="form-inline">
                        <div className="form-group">
                           <label>Fecha: </label>
                           <DatePicker
                              selected={this.state.fechaComanda}
                              onChange={this.handleChangeFechaComandas}
                              dateFormat="dd/MM/yyyy"
                              className="form-control col-md-8"
                              />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={this.mostrarComandas}>Mostrar</button>
                        <Table className="mt-4">
                           <thead>
                              <tr>
                                 <th>Garzon</th>
                                 <th>Total</th>
                                 <th>Comision(5%)</th>
                                 <th></th>
                              </tr>
                           </thead>
                           <tbody>
                              {comandaList}
                           </tbody>
                        </Table>
                        
                     </form>
                  </TabPanel>
               </Tabs>
            </Container>
         </div>
      )
   }
}

export default IngresosEgresosList;