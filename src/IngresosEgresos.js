import React, { Component } from 'react';
import AppNavbar from './AppNavbar';
import { Container, Table } from 'reactstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { server } from './servercontext';

class IngresosEgresos extends Component {
   constructor(props){
      super(props);
      this.state = {
         ingresos: [],
         egresos: [],
         isLoading: false,
         fecha: new Date(),
         totalEgresos: 0,
         totalIngresos: 0
      };

      this.MostrarEgresos = this.MostrarEgresos.bind(this);
      this.handleChangeFecha = this.handleChangeFecha.bind(this);
   }
   
   componentDidMount() {
      this.setState({isLoading: true});
      this.MostrarEgresos();
   }

   handleChangeFecha(date1){
      this.setState({fecha: date1});
   }

   MostrarEgresos(){
      const parametro = {
         fechaInicio: this.state.fecha
      };
      this.setState({isLoading: true});
      axios.post(`${server.ip}/gasto/findByFecha`, parametro)
         .then(res => {
            this.setState({egresos: res.data, isLoading: false});
            var totalEgresos1 = 0;
            for(var i = 0; i < this.state.egresos.length; i++){
               totalEgresos1 += this.state.egresos[i].monto;
            }
            this.setState({totalEgresos: totalEgresos1});
         });

      axios.post(`${server.ip}/ComandasMeseros/obtenerTotal`, parametro)
         .then(res => {
            this.setState({totalIngresos: res.data});
         });
   }

   render() {
      const {egresos, ingresos, isLoading, totalEgresos, totalIngresos} = this.state;

      if(isLoading) {
         return <p>Cargando</p>;
      }

      const egresoList = egresos.map(eg => {
         return <tr key={eg.gastoId}>
            <td>{eg.tipoGastoDesc}</td>
            <td>{eg.monto}</td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
            <br/>
               <form className="form-inline">
                        <div className="form-group">
                           <label>Fecha: </label>
                           <DatePicker
                              selected={this.state.fecha}
                              onChange={this.handleChangeFecha}
                              dateFormat="dd/MM/yyyy"
                              className="form-control col-md-8"
                              />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={this.MostrarEgresos}>Mostrar</button>
                        
                     </form>
               <h3>Ingresos</h3>
               <Table className="mt-4">
                  <thead>
                     <tr>
                        <th>Concepto</th>
                        <th>Monto</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Venta Garzones</td>
                        <td>{totalIngresos}</td>
                     </tr>
                  </tbody>
               </Table>
               <h3>Egresos</h3>
               <Table className="mt-4">
                  <thead>
                     <tr>
                        <th>Concepto</th>
                        <th>Monto</th>
                     </tr>
                  </thead>
                  <tbody>
                     {egresoList}
                     <tr>
                        <td><b>Total:</b> </td>
                        <td><b>{totalEgresos}</b> </td>
                     </tr>
                  </tbody>
               </Table>
            </Container>
         </div>
      )
   }
}

export default IngresosEgresos;