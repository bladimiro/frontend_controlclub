import React, { Component } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink, UncontrolledDropdown,
    DropdownToggle, DropdownMenu, DropdownItem  } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { server } from './servercontext';

export default class AppNavbar extends Component {
    emptyItem = {
		usuarioId: '',
		username: '',
		password: ''
	};
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false, 
            opciones: [], 
            usuario: this.emptyItem,
            username: ''
        };
        this.loggedIn = sessionStorage.getItem('loggedin') === 'true';
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        if(localStorage.getItem('user') !== null){
            this.setState({username: JSON.parse(localStorage.getItem('user')).username});
            axios.get(`${server.ip}/PerfilesUsuarios/findOpcionesPrincipalesByUsuarioId/${JSON.parse(localStorage.getItem('user')).usuarioId}`)
            .then(res => {
                this.setState({ opciones: res.data });
            });
        } 
        
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        const {opciones, username} = this.state;
        
        const opcionList = opciones.map(opc => {
            return <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                    {opc.nombre}
                </DropdownToggle>
                <DropdownMenu right>
                {opc.subopciones.map(so => <DropdownItem><Link to={so.enlace}>{so.nombre}</Link></DropdownItem>)}
                </DropdownMenu>
            </UncontrolledDropdown>
        });
        return <Navbar color="primary" dark expand="md">
        <NavbarBrand tag={Link} to="/">Control Club</NavbarBrand>
        <NavbarToggler onClick={this.toggle}/>
        <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
                {opcionList}
                <NavItem>
                    <NavLink
                        href="#">
                        ({username})
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink>
                        <Link style={{ color: '#FFF' }} to="/login">Cerrar sesion</Link>
                    </NavLink>
                    
                </NavItem>
            </Nav>
        </Collapse>
    </Navbar>;
    }
}