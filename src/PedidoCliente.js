import React, { Component } from 'react';
import { Container, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import axios from 'axios';
import { server } from './servercontext';

class Pedidos extends Component {
	constructor(props) {
		super(props);
		this.state = {
			subproductos: [], 
			seleccionados: [], 
			mesas: [],
			isLoading: true, 
			mesa: '', 
			monto: 0, 
			montoTotal: 0,
			modalDisplay: false,
			codigo: '',
			chicas: []
		};

		this.restarPedido = this.restarPedido.bind(this);
		this.adicionarPedido = this.adicionarPedido.bind(this);
		this.enviarPedido = this.enviarPedido.bind(this);
		this.handleChangeMesa = this.handleChangeMesa.bind(this);
		this.handleChangeMonto = this.handleChangeMonto.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.desplegarModal = this.desplegarModal.bind(this);
		this.handleChangeGirl = this.handleChangeGirl.bind(this);
	}

	componentDidMount() {
      this.setState({isLoading: true});
      // .../botella/cliente
		axios.get(`${server.ip}/Subproducto/findByTipoIdTiSolicitante/1009/1013`)
         .then(res => {
				var datos = res.data;
				for(var i = 0; i < datos.length; i++) {
					datos[i].pedidoDamas = [];
				}
				this.setState({ subproductos: datos, isLoading: false });
			});

		axios.get(`${server.ip}/Mesa/findAllActives`)
			.then(res => {
				this.setState({mesas: res.data})
			});
		
	}

	desplegarModal(event) {
			
		//if(this.state.subproductos.length)
			for(var i = 0; i < this.state.subproductos.length; i++){
				if(this.state.subproductos[i].cantidad > 0 ){
					this.state.seleccionados.push(this.state.subproductos[i]);
				}
			}
			if(this.state.seleccionados.length > 0) {
				//console.log(this.state.seleccionados)
				this.setState({modalDisplay: !this.state.modalDisplay});
			} else {
				confirmAlert({
					title: 'Advertencia',
					message: 'No ha seleccionado ninguna bebida.',
					buttons: [
						{
							label: 'Aceptar',
							onClick: () => {}
						}
					]
				})
			}
		
	}

	handleChangeGirl(event){
		this.setState({codigo: event.target.value});
	}

	handleChangeMesa(event) {
		const target = event.target;
		const value = target.value;
      let mesa = {...this.state.mesa};
      mesa = value;
      this.setState({mesa});
	}

	handleChangeMonto(event) {
		const target = event.target;
		const value = target.value;
      let monto = {...this.state.monto};
      monto = value;
      this.setState({monto});
	}

	handleChange(event) {
		const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
		item[name] = value;
		if(name === "codigo"){
			this.setState({codigo: value});
		}
      this.setState({item});
	 }

	async enviarPedido(event) {
		console.log("envio");
		if(this.state.seleccionados.length > 0) {
			const respuesta = {
				numeroPedido: '',
				mensaje: ''
			};
			
			const datosPedido = {
				clienteId: 0,
				mesaId: this.state.mesa,
				usuarioId: JSON.parse(localStorage.getItem('user')).usuarioId,
				montoTotal: this.state.montoTotal,
				montoEntregado: 0,
				tipoPagoId: 1005,
				subproductos: this.state.seleccionados
			};

			axios.post(`${server.ip}/Pedido/enviarPedido`, datosPedido)
				.then(res => {
					console.log(res.data);
				});

			/*confirmAlert({
				title: 'Realizar pedido',
				message: 'Confirma enviar el Pedido?',
				buttons: [
					{
						label: 'Si',
						onClick: () => alert('Si')
					},
					{
						label: 'No',
						onClick: () => alert('No')
					}
				]
			})*/
		} else {
			alert('No ha seleccionado alguna Bebida');
		}
		//console.log(this.state.seleccionados);
		
		
	}

	restarPedido(id) {
		let indexSubpro = this.state.subproductos.findIndex(p => p.subproductoId === id);
		var statusCopy = Object.assign({}, this.state);
		
		if(statusCopy.subproductos[indexSubpro].cantidad > 0) {
			statusCopy.subproductos[indexSubpro].cantidad -= 1;
			statusCopy.montoTotal -= statusCopy.subproductos[indexSubpro].precio;
			this.setState(statusCopy);
		}
	}

	adicionarPedido(id) {
		//var subpro = this.state.subproductos.find(p => p.subproductoId === id);
		let indexSubpro = this.state.subproductos.findIndex(p => p.subproductoId === id);
		//console.log(indexSubpro);
		var statusCopy = Object.assign({}, this.state);
		statusCopy.montoTotal += statusCopy.subproductos[indexSubpro].precio;
		statusCopy.subproductos[indexSubpro].cantidad += 1;
		this.setState(statusCopy);
	}

	adicionarDama(id){
		let indexSubpro = this.state.subproductos.findIndex(p => p.subproductoId === id);
		//console.log(this.state.codigo);
		var statusCopy = Object.assign({}, this.state);
		var cantidadRegistrada = statusCopy.subproductos[indexSubpro].pedidoDamas.length;

		if((cantidadRegistrada < statusCopy.subproductos[indexSubpro].cantidadChicas) &&
			(statusCopy.subproductos[indexSubpro].cantidadChicas !== 0)){
				axios.get(`${server.ip}/Damas/findByCodigo/${this.state.codigo}`)
					.then(res => {
						console.log(res.data);
						const pedidoDama = {
							damaId: res.data.damaId,
							nombre: res.data.nombre
						}
						//var statusCopy = Object.assign({}, this.state);		
						statusCopy.subproductos[indexSubpro].codigo = null;
						
						var pedDama = statusCopy.subproductos[indexSubpro].pedidoDamas;
						pedDama.push(pedidoDama);
						statusCopy.subproductos[indexSubpro].pedidoDamas = pedDama;
						this.setState(statusCopy);
						this.setState({codigo: null});
					});
		} else {
			alert("No puede asociar chicas");
		}
	}

	render() {
		const {subproductos, isLoading, montoTotal, modalDisplay} = this.state;
		//, mesa, mesas,  codigo

		if(isLoading) {
			return <p>Cargando...</p>;
		}

		const subproductoList = subproductos.map(subp => {
			return <tr key={subp.subproductoId}>
				<td>{subp.nombre}</td>
				<td>{subp.precio}</td>
				<td>
					<Button type="button" color="success" onClick={() => this.adicionarPedido(subp.subproductoId)}>+</Button>
					<Button type="button" color="warning" onClick={() => this.restarPedido(subp.subproductoId) }>-</Button>
				</td>
				<td>
					<input type="text" name="cantidad" value={subp.cantidad} className="form-control col-md-3" readOnly />
				</td>
				<td>
					<input type="text" name="codigo" value={subp.codigo} className="form-control col-md-3"
							onChange={this.handleChange} />
					<Button type="button" color="success" onClick={() => this.adicionarDama(subp.subproductoId)}>>></Button>
					<ul>
						{subp.pedidoDamas.map(d => <li key={d.damaId}>{d.nombre}</li>)}
					</ul>
				</td>
			</tr>
		});

		const listado2 = subproductos.map(sub => {
			return <div role="alert" aria-live="assertive" aria-atomic="true" class="shadow p-3 mb-5 bg-white rounded" data-autohide="false">
				<div class="toast-header">
					<strong class="mr-auto">{sub.nombre} - Bs. {sub.precio}</strong>
				</div>
				<div class="toast-body">
					<div class="input-group">
						<Button type="button" color="success" onClick={() => this.adicionarPedido(sub.subproductoId)}>Agregar</Button>
						<Button type="button" color="warning" onClick={() => this.restarPedido(sub.subproductoId) }>Restar</Button>
						<input type="text" name="cantidad" value={sub.cantidad} className="form-control col-md-3" readOnly />
					</div>
				</div>
				<br/>
				<div class="toast-footer">
					<div class="input-group">
						<input type="text" name="codigo" value={sub.codigo} className="form-control col-md-3"
								placeholder="Ingrese codigo de Lady" onChange={this.handleChange} />
						<Button type="button" color="success" onClick={() => this.adicionarDama(sub.subproductoId)}>Buscar</Button>
						
					</div>
					<ul>
						{sub.pedidoDamas.map(d => <li key={d.damaId}>{d.nombre}</li>)}
					</ul>
				</div>
			</div>
		});

		/*const mesaList = mesas.map(mesa => {
			return <option value={mesa.mesaId}>{mesa.numero}</option>
		});*/
		
		/*<div className="row">
                           <div className="form-group col-md-8">
                              <label>Seleccione Mesa:</label>
										<select name="mesa" value={mesa} onChange={this.handleChange} className="form-control">
											<option value=''></option>
											{mesaList}
										</select>
                           </div>
                        </div> */
		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<Modal isOpen={modalDisplay}>
						<form onSubmit={this.enviarPedido}>
							<ModalHeader>Confirmar Pedido</ModalHeader>
							<ModalBody>
								<div className="row">
                           <div className="form-group col-md-8">
                              <label>Monta Total:</label>
                              <input type="text" name="montoTotal" readOnly value={montoTotal} className="form-control" />
                           </div>
                        </div>
								
							</ModalBody>
							<ModalFooter>
								<input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
								<Button color="danger" onClick={this.toggle}>Cancelar</Button>
							</ModalFooter>
						</form>
					</Modal>
					<h3>Listado de Bebidas</h3>
					<form>
						{listado2}
						<div className="row">
							<div className="form-group col-md-6">
								<label><b>Monto Total (Bs): {montoTotal}</b> </label>
							</div>
						</div>
						<div className="row">
							<div className="form-group col-md-6">
								<input type="button" value="Pedir" color="primary" className="btn btn-primary" onClick={this.desplegarModal}/>
							</div>
						</div>
					</form>
				
				</Container>
			</div>
		)
	}
}

export default Pedidos;