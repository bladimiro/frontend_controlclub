import React, { Component } from 'react';
import { Container } from 'reactstrap';
import AppNavbar from './AppNavbar';

class DatosEmpresa extends Component {
	defaultItem = {
		empresaId: '',
		nombre: '',
		direccion: '',
		telefono1: '',
		telefono2: '',
		correoElectronico: ''
	};

	constructor(props) {
		super(props);
		this.state = {empresa: this.defaultItem};
		//, isLoading: true
		this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		this.recuperarDatos();
	}

	async recuperarDatos(){
		const datosempresa = await(await fetch('/empresa/findFirst')).json();
		//console.log(datosempresa);
		
		this.setState({empresa: datosempresa});
	}

	handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
	}
	
	async guardarDatos(event) {
		event.preventDefault();
		const {item} = this.state.empresa;
		//console.log(item);
		
		await fetch('/empresa/insertOrUpdate', {
			method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
		})
	}

	render() {
		const {empresa} = this.state;

		/*if(isLoading) {
			return <p>Cargando...</p>;
		}*/

		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<form onSubmit={this.guardarDatos}>
						<div className="row">
							<div className="form-group col-md-8">
								<label>Nombre:</label>
								<input type="text" readOnly name="nombre" value={empresa.nombre} className="form-control"
                              onChange={this.handleChange} />
							</div>
						</div>
						<div className="row">
							<div className="form-group col-md-8">
								<label>Dirección:</label>
								<input type="text" readOnly name="direccion" value={empresa.direccion} className="form-control"
                              onChange={this.handleChange} />
							</div>
						</div>
						<div className="row">
							<div className="form-group col-md-8">
								<label>Teléfono 1:</label>
								<input type="text" readOnly name="telefono1" value={empresa.telefono1} className="form-control"
                              onChange={this.handleChange} />
							</div>
						</div>
						<div className="row">
							<div className="form-group col-md-8">
								<label>Teléfono 2:</label>
								<input type="text" readOnly name="telefono2" value={empresa.telefono2} className="form-control"
                              onChange={this.handleChange} />
							</div>
						</div>
						<div className="row">
							<div className="form-group col-md-8">
								<label>Correo Electrónico:</label>
								<input type="text" readOnly name="correoElectronico" value={empresa.correoElectronico} className="form-control"
                              onChange={this.handleChange} />
							</div>
						</div>
					</form>
					
				</Container>
			</div>
		)
	}
}

export default DatosEmpresa;