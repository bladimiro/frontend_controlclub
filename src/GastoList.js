import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import DatePicker from "react-datepicker";
import Moment from 'react-moment';
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { server } from './servercontext';

class GastoList extends Component {
   emptyItem = {
      gastoId: '',
      tipoGastoId: '',
      descripcion: '',
      fechaGasto: '',
      monto: '',
      usuarioId: ''
   };

   constructor(props) {
      super(props);
      this.state = {
         gastos: [], 
         tiposgastos: [], 
         isLoading: false, 
         item: this.emptyItem,
         startDate: new Date()
      };
      this.remove = this.remove.bind(this);

		// metodos modal
      this.toggle = this.toggle.bind(this);
      this.nuevoGasto = this.nuevoGasto.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleChangeDate = this.handleChangeDate.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/gasto/findAll`)
         .then(res => {
            this.setState({gastos: res.data, isLoading: false});
         });

      axios.get(`${server.ip}/tipoGasto/findAll`)
         .then(res => {
            this.setState({tiposgastos: res.data});
         });
      /*fetch('/gasto/findAll')
         .then(response => response.json())
         .then(data => this.setState({gastos: data, isLoading: false}));
      
      fetch('/tipoGasto/findAll')
         .then(response => response.json())
         .then(data => this.setState({tiposgastos: data}));*/
   }

   handleChangeDate(date1){
      this.setState({ startDate: date1 });
   }

   handleChange(event) {
		const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }
   
   toggle() {
      this.setState({modal: !this.state.modal});
   }

   nuevoGasto() {
      this.toggle();
      this.setState({item: this.emptyItem});
   }

   async guardarDatos(event) {
		event.preventDefault();
      const {item} = this.state;
      item.fechaGasto = this.state.startDate;
      item.usuarioId = JSON.parse(localStorage.getItem('user')).usuarioId;

      await axios.post(`${server.ip}/gasto/insertOrUpdate`, item)
         .then(res => {
            this.setState({item: this.emptyItem});
            this.componentDidMount();
         });
      /*fetch('/gasto/insertOrUpdate', {
			method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
		});*/

		
      this.toggle();
	}

	async remove(id) {
      await axios.get(`${server.ip}/gasto/deleteById/${id}`)
         .then(res => {
            let updatedGastos = [...this.state.gastos].filter(i => i.gastoId !== id);
			   this.setState({gastos: updatedGastos});
         })
      /*fetch(`/gasto/deleteById/${id}`,{
			method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
		}).then(() => {
			let updatedGastos = [...this.state.gastos].filter(i => i.gastoId !== id);
			this.setState({gastos: updatedGastos});
		});*/
   }
   
   async editar(id){
      await axios.get(`${server.ip}/gasto/findById/${id}`)
         .then(res => {
            this.setState({item: res.data});
		      this.toggle();
         });
      /*const gasto = await(await fetch(`/gasto/findById/${id}`)).json();
		this.setState({item: gasto});
		this.toggle();*/
   }

   render() {
      const {gastos, isLoading, item, tiposgastos} = this.state;
      const titulo = <h3>{item.gastoId ? 'Editar Gasto' : 'Adicionar Gasto'}</h3>;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const gastoList = gastos.map(gasto => {
         return <tr key={gasto.gastoId}>
            <td>{gasto.tipoGastoDesc}</td>
            <td>
               <Moment format="DD/MM/YYYY">
                  {gasto.fechaGasto}
               </Moment>
            </td>
            <td>{gasto.monto}</td>
            <td>{gasto.descripcion}</td>
            <td>
               <ButtonGroup>
                  <Button size="sm" color="primary" onClick={() => this.editar(gasto.gastoId)}>Editar</Button>
                  <Button size="sm" color="danger" onClick={() => this.remove(gasto.gastoId)}>Borrar</Button>
               </ButtonGroup>
            </td>
         </tr>
      });

      const tiposList = tiposgastos.map(tip => {
         return <option value={tip.tipoGastoId}>{tip.descripcion}</option>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarDatos}>
                     <ModalHeader>{titulo}</ModalHeader>
                     <ModalBody>
                        <div className="row">
									<div className="form-group col-md-8">
                              <label>Tipo Gasto:</label>
                              <select name="tipoGastoId" value={item.tipoGastoId} onChange={this.handleChange} className="form-control">
                                 <option value=''></option>
                                 {tiposList}
                              </select>
                           </div>
								</div>
                        <div className="row">
									<div className="form-group col-md-8">
                              <label>Fecha Gasto:</label>
                              <DatePicker
                                 selected={this.state.startDate}
                                 onChange={this.handleChangeDate}
                                 dateFormat="dd/MM/yyyy"
                                 className="form-control"
                                 />
                             
                           </div>
								</div>
                        <div className="row">
									<div className="form-group col-md-8">
                              <label>Monto:</label>
                              <input type="text" name="monto" value={item.monto} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
								</div>
                        <div className="row">
									<div className="form-group col-md-8">
                              <label>Descripción:</label>
                              <input type="text" name="descripcion" value={item.descripcion} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
								</div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Gastos</h3>
               <Button color="success" onClick={this.nuevoGasto}>Adicionar</Button>
               <Table className="mt-4">
                  <thead>
                     <tr>
                        <th>Tipo Gasto</th>
                        <th>Fecha Gasto</th>
                        <th>Monto</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                     </tr>
                  </thead>
                  <tbody>
                     {gastoList}
                  </tbody>
               </Table>
            </Container>
         </div>
      )
   }
}

export default GastoList;