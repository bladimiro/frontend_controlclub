// ModalComponent.js
import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class ModalComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {modal: false, name: '', price: ''};

        this.toggle = this.toggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleSubmit(event) {
        console.log("guardar");
        
        event.preventDefault();
        this.toggle();
    }

    render() {
        return (
            <div>
                
                <Button color="success" onClick={this.toggle}>Adicionar</Button>
                <Modal isOpen={this.state.modal}>
                    <form onSubmit={this.handleSubmit}>
                        <ModalHeader>Producto</ModalHeader>
                        <ModalBody>
                            <div className="row">
                                <div className="form-group col-md-8">
                                    <label>Nombre:</label>
                                    <input type="text" value={this.state.name} className="form-control" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-md-8">
                                    <label>Precio:</label>
                                    <input type="text" value={this.state.price} className="form-control" />
                                </div>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                            <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                        </ModalFooter>
                    </form>
                </Modal>
            </div>
        )
    }
}