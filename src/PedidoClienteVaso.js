import React, { Component } from 'react';
import { Container, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import axios from 'axios';
import { server } from './servercontext';
import add from './add1.png';
import sub1 from './sub.png';

class PedidoClienteVaso extends Component {
	emptyItem = {
		pedidoId: '',
		numeroPedido: '',
		montoTotal: '',
		estadoPedidoId: ''
	};
	constructor(props) {
		super(props);
		this.state = {
			subproductos: [], 
			seleccionados: [], 
			mesas: [],
			isLoading: true, 
			mesa: '', 
			monto: 0, 
			montoTotal: 0,
			modalDisplay: false,
			codigo: '',
			item: this.emptyItem,
			chicas: [],
			existeRegistro: false,
			detallesPedido: []
		};

		this.toggle = this.toggle.bind(this);
		this.restarPedido = this.restarPedido.bind(this);
		this.adicionarPedido = this.adicionarPedido.bind(this);
		this.enviarPedido = this.enviarPedido.bind(this);
		this.handleChangeMesa = this.handleChangeMesa.bind(this);
		this.handleChangeMonto = this.handleChangeMonto.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.desplegarModal = this.desplegarModal.bind(this);
		this.handleChangeGirl = this.handleChangeGirl.bind(this);
		this.guardarDatos = this.guardarDatos.bind(this);
	}

	componentDidMount() {
      this.setState({isLoading: true});
      // .../vaso/cliente
		axios.get(`${server.ip}/Subproducto/findByTipoIdTiSolicitante/1010/1013`)
         .then(res => {
				var datos = res.data;
				for(var i = 0; i < datos.length; i++) {
					datos[i].pedidoDamas = [];
				}
				this.setState({ subproductos: datos, isLoading: false });
			});

		axios.get(`${server.ip}/Pedido/findByUsuarioIdEstadoPedidoId/${JSON.parse(localStorage.getItem('user')).usuarioId}`)
			.then(res => {
				this.setState({item: res.data, existeRegistro: true});
			});
	}

	toggle(){
		this.setState({modalDisplay: !this.state.modalDisplay});
	}

	desplegarModal(event) {
		this.setState({modalDisplay: !this.state.modalDisplay});
		axios.get(`${server.ip}/DetallePedido/findByPedidoId/${this.state.item.pedidoId}`)
			.then(res => {
				this.setState({detallesPedido: res.data});
			});
		
	}

	async guardarDatos(){
		for(var i = 0; i < this.state.subproductos.length; i++){
			if(this.state.subproductos[i].cantidad > 0 ){
				this.state.seleccionados.push(this.state.subproductos[i]);
			}
		}
		if(this.state.seleccionados.length > 0) {

			if(!this.state.existeRegistro){
				const datosPedido = {
					clienteId: 0,
					usuarioId: JSON.parse(localStorage.getItem('user')).usuarioId,
					montoTotal: this.state.montoTotal,
					montoEntregado: 0,
					tipoPagoId: 1005,
					subproductos: this.state.seleccionados
				};
				await axios.post(`${server.ip}/Pedido/guardarPedido`, datosPedido)
				.then(res => {
					//console.log(res.data);
					this.setState({seleccionados: []});
					this.componentDidMount();
				});
			} else {
				const datosPedido = {
					pedidoId: this.state.item.pedidoId,
					clienteId: 0,
					usuarioId: JSON.parse(localStorage.getItem('user')).usuarioId,
					montoTotal: this.state.item.montoTotal + this.state.montoTotal,
					montoEntregado: 0,
					subproductos: this.state.seleccionados
				};
				await axios.post(`${server.ip}/Pedido/agregarPedido`, datosPedido)
					.then(res => {
						this.setState({seleccionados: []});
						this.componentDidMount();
					})
			}
			

		} else {
			confirmAlert({
				title: 'Advertencia',
				message: 'No ha seleccionado ninguna bebida.',
				buttons: [
					{
						label: 'Aceptar',
						onClick: () => {}
					}
				]
			})
		}
	}

	handleChangeGirl(event){
		this.setState({codigo: event.target.value});
	}

	handleChangeMesa(event) {
		const target = event.target;
		const value = target.value;
      let mesa = {...this.state.mesa};
      mesa = value;
      this.setState({mesa});
	}

	handleChangeMonto(event) {
		const target = event.target;
		const value = target.value;
      let monto = {...this.state.monto};
      monto = value;
      this.setState({monto});
	}

	handleChange(event) {
		const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
		item[name] = value;
		if(name === "codigo"){
			this.setState({codigo: value});
		}
      this.setState({item});
	 }

	async enviarPedido(event) {
		const datosPedido = {
			pedidoId: this.state.item.pedidoId,
			usuarioId: JSON.parse(localStorage.getItem('user')).usuarioId
		};
		this.setState({isLoading: true});
		axios.post(`${server.ip}/Pedido/consolidarPedido`, datosPedido)
				.then(res => {
					this.setState({
						isLoading: false, 
						item: this.emptyItem, 
						existeRegistro: false, 
						montoTotal: 0,
						modalDisplay: !this.state.modalDisplay});
					this.componentDidMount();				
				});
	}

	restarPedido(id) {
		let indexSubpro = this.state.subproductos.findIndex(p => p.subproductoId === id);
		var statusCopy = Object.assign({}, this.state);
		
		if(statusCopy.subproductos[indexSubpro].cantidad > 0) {
			statusCopy.subproductos[indexSubpro].cantidad -= 1;
			statusCopy.montoTotal -= statusCopy.subproductos[indexSubpro].precio;
			this.setState(statusCopy);
		}
	}

	adicionarPedido(id) {
		let indexSubpro = this.state.subproductos.findIndex(p => p.subproductoId === id);
		
		var statusCopy = Object.assign({}, this.state);
		statusCopy.montoTotal += statusCopy.subproductos[indexSubpro].precio;
		statusCopy.subproductos[indexSubpro].cantidad += 1;
		this.setState(statusCopy);
	}

	adicionarDama(id){
		let indexSubpro = this.state.subproductos.findIndex(p => p.subproductoId === id);
		//console.log(this.state.codigo);
		var statusCopy = Object.assign({}, this.state);
		var cantidadRegistrada = statusCopy.subproductos[indexSubpro].pedidoDamas.length;

		if((cantidadRegistrada < statusCopy.subproductos[indexSubpro].cantidadChicas) &&
			(statusCopy.subproductos[indexSubpro].cantidadChicas !== 0)){
				axios.get(`${server.ip}/Damas/findByCodigo/${this.state.codigo}`)
					.then(res => {
						console.log(res.data);
						const pedidoDama = {
							damaId: res.data.damaId,
							nombre: res.data.nombre
						}
						//var statusCopy = Object.assign({}, this.state);		
						statusCopy.subproductos[indexSubpro].codigo = null;
						
						var pedDama = statusCopy.subproductos[indexSubpro].pedidoDamas;
						pedDama.push(pedidoDama);
						statusCopy.subproductos[indexSubpro].pedidoDamas = pedDama;
						this.setState(statusCopy);
						this.setState({codigo: null});
					});
		} else {
			alert("No puede asociar chicas");
		}
	}

	render() {
		const {subproductos, isLoading, montoTotal, modalDisplay, detallesPedido, existeRegistro, item} = this.state;

		if(isLoading) {
			return <p>Cargando...</p>;
		}

		const detalleList = detallesPedido.map(det => {
			return <tr key={det.detallePedidoId}>
				<td width="20%" align="center">{det.cantidad}</td>
				<td width="60%">
				{det.nombreSubproducto}
				{det.damasConcatenadas}
				</td>
				<td width="20%" align="right">{det.precioUnitario}</td>
			</tr>
		});

		const listado2 = subproductos.map(sub => {
			return <div role="alert" aria-live="assertive" aria-atomic="true" class="shadow p-3 mb-5 bg-white rounded" data-autohide="false">
				<div class="toast-header">
					<strong class="mr-auto">{sub.nombre} - Bs. {sub.precio}</strong>
				</div>
				<div class="toast-body">
					<div class="input-group">
						<Button type="button" color="success" onClick={() => this.adicionarPedido(sub.subproductoId)}>
							<img src={add} />
						</Button>
						<Button type="button" color="warning" onClick={() => this.restarPedido(sub.subproductoId) }>
							<img src={sub1} />
						</Button>
						<input type="text" name="cantidad" value={sub.cantidad} className="form-control col-md-3" readOnly />
					</div>
				</div>
			</div>
      });
      
		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<Modal isOpen={modalDisplay}>
						<form onSubmit={this.enviarPedido}>
							<ModalHeader>Confirmar Pedido</ModalHeader>
							<ModalBody>
								<div className="row">
                           <div className="form-group col-md-8">
                              <label>Numero Pedido: </label>{item.numeroPedido}
                           </div>
                        </div>
								<table>
									<thead>
										<tr>
											<td><b>Cant.</b></td>
											<td><b>Producto</b></td>
											<td><b>Precio</b></td>
										</tr>
									</thead>
									<tbody>
										{detalleList}
										<tr>
											<td></td>
											<td><b>Total:</b> </td>
											<td align="right"><b>{item.montoTotal} </b></td>
										</tr>
									</tbody>
								</table>
							</ModalBody>
							<ModalFooter>
								<input type="submit" value="Enviar" color="primary" className="btn btn-primary"/>
								<Button color="danger" onClick={this.toggle}>Cerrar</Button>
							</ModalFooter>
						</form>
					</Modal>
					<h3>Pedidos Vaso Cliente</h3>
					<form>
						{listado2}
						<div className="row">
							<div className="form-group col-md-6">
								<label><b>Monto Total (Bs): {montoTotal}</b> </label>
							</div>
						</div>
						<div className="row">
							<div className="form-group col-md-6">
								<input type="button" value="Agregar" color="success" className="btn btn-success" onClick={this.guardarDatos}/>
								{ existeRegistro ? <input type="button" value="Pedir" color="primary" className="btn btn-primary" onClick={this.desplegarModal}/> : null}
							</div>
						</div>
					</form>
				
				</Container>
			</div>
		)
	}
}

export default PedidoClienteVaso;