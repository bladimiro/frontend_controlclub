import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import { server } from './servercontext';

class ChicasList extends Component {
   emptyItem = {
      damaId: '',
      codigo: '',
      nombre: ''
   };

   constructor(props){
      super(props);
      this.state = {
         chicas: [],
         isLoading: false,
         item: this.emptyItem
      };
      this.toggle = this.toggle.bind(this);
      this.nuevo = this.nuevo.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.cambiarEstado = this.cambiarEstado.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/Damas/findByActives`)
         .then(res => {
            this.setState({chicas: res.data, isLoading: false});
         });
   }

   handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }

   async cambiarEstado(id, estado){
      await axios.get(`${server.ip}/Damas/cambiarEstado/${id}/${estado}`)
         .then(res => {
            let updatedChicas = [...this.state.chicas];
            let index = updatedChicas.findIndex(p => p.damaId === id);
            updatedChicas[index].habilitada = estado;
            this.setState({chicas: updatedChicas});
         });
   }

   mostrarEstado(estado){
      if(estado === true){
         return "Habilitada";
      } else {
         return "Deshabilitada";
      }
   }

   mostrarEstadoBoton(estado) {
      if(estado === true) {
         return "Deshabilitar";
      } else {
         return "Habilitar";
      }
   }

   toggle() {
      this.setState({modal: !this.state.modal});
   }

   async guardarDatos(event) {
      event.preventDefault();
      const {item} = this.state;

      await axios.post(`${server.ip}/Damas/insertOrUpdate`, item)
         .then(res => {
            this.setState({item: this.emptyItem});
            this.componentDidMount();
            this.toggle();
         });
   }

   nuevo(){
      this.toggle();
      this.setState({item: this.emptyItem});
   }

   async editar(id) {
      await axios.get(`${server.ip}/Damas/findById/${id}`)
         .then(res => {
            this.setState({item: res.data});
            this.toggle();
         })
   }

   render() {
      const {chicas, isLoading, item} = this.state;
      const titulo = <h3>{item.damaId ? 'Editar Datos' : 'Adicionar Datos'}</h3>;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const chicaList = chicas.map(chi => {
         return <tr key={chi.damaId}>
            <td>{chi.codigo}</td>
            <td>{chi.nombre}</td>
            <td>{this.mostrarEstado(chi.habilitada)}</td>
            <td>
               <ButtonGroup>
                  <Button size="sm" color="default"
                     onClick={() => this.cambiarEstado(chi.damaId, !chi.habilitada)}>
                     {this.mostrarEstadoBoton(chi.habilitada)}
                  </Button>
                  <Button size="sm" color="primary" onClick={() => this.editar(chi.damaId)}>Editar</Button>
               </ButtonGroup>
            </td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarDatos}>
                     <ModalHeader>{titulo}</ModalHeader> 
                     <ModalBody>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Codigo:</label>
                              <input type="text" name="codigo" value={item.codigo} className="form-control"
                                 onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Nombre:</label>
                              <input type="text" name="nombre" value={item.nombre} className="form-control"
                                 onChange={this.handleChange} />
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Chicas</h3>
               <Button color="success" onClick={this.nuevo}>Adicionar</Button>
               <Table>
                  <thead>
                     <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                     </tr>
                  </thead>
                  <tbody>
                     {chicaList}
                  </tbody>
               </Table>
            </Container>
         </div>
      );
   }
}

export default ChicasList;