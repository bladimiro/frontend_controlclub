import React, { Component } from 'react';
import { Container } from 'reactstrap';
import AppNavbar from './AppNavbar2';
import axios from 'axios';
import "./Login.css";
import { server } from './servercontext';

class Login extends Component {
	emptyItem = {
		usuarioId: '',
		username: '',
		password: ''
	};

	constructor(props) {
		super(props);
		this.state = {usuario: this.emptyItem}
		this.validarDatos = this.validarDatos.bind(this);
		this.handleChange = this.handleChange.bind(this);
		sessionStorage.setItem('loggedin', 'false');
	}

	componentDidMount() {
		//this.clearData();
	}

	clearData(){
		this.setState({usuario: this.emptyItem});
	}

	handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let usuario = {...this.state.usuario};
      usuario[name] = value;
      this.setState({usuario});
	}

	async validarDatos(event) {
		event.preventDefault();
		const {usuario} = this.state;
		
		const user = {
			username: usuario.username,
			password: usuario.password
		};
		axios.post(`${server.ip}/usuario/verifyLogin`, user)
			.then(res => {
				console.log(res.data);
				localStorage.setItem('user', JSON.stringify(res.data));
				sessionStorage.setItem('loggedin', 'true');
				this.props.history.push("/pedidos");
			})
			.catch(function (error) {
				console.log(error);
			 });
		//try {
			/*await fetch('/usuario/verifyLogin', {
				method: 'POST',
					headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(usuario),
			}) 
			//.then(response => response.json())
			.then(data => {
				localStorage.setItem('user', JSON.stringify(data));
				sessionStorage.setItem('loggedin', 'true');
				this.props.history.push("/pedidos");
			})
			.catch(error => {
				console.error(error);
				alert("Datos incorrectos");
				//return { name: "network error", description: "" };
			});*/
		/*} catch(e) {
			alert(e.message);
		}*/
		
	}

	render() {
		const {usuario} = this.state;

		return (
			<div>
				<AppNavbar/>
				<Container fluid>
					<div className="Login">
						<form onSubmit={this.validarDatos}>
							<div className="row">
								<div className="form-group col-md-10">
									<label>Login:</label>
									<input type="text" name="username" value={usuario.username} className="form-control"
													onChange={this.handleChange} placeholder="Ingrese Login" />
								</div>
							</div>
							<div className="row">
								<div className="form-group col-md-10">
									<label>Contraseña:</label>
									<input type="password" name="password" value={usuario.password} className="form-control"
										onChange={this.handleChange} placeholder="Ingrese Contraseña" />
								</div>
							</div>
							<div className="row">
								<div className="form-group col-md-5">
									<input type="submit" value="Ingresar" color="primary" className="btn btn-primary"/>
								</div>
							</div>
						</form>
					</div>
					
				</Container>
			</div>
		)
	}
}

export default Login;