import React, { Component } from 'react';
import { Container, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import Moment from 'react-moment';
import { server } from './servercontext';

class PedidosPendientes extends Component {
   emptyItem = {
      pedidoId: '',
      nombreCliente: '',
      montoTotal: 0,
      montoEntregado: 0,
      montoCambio: 0
   }
   constructor(props){
      super(props);
      this.state = {
         pedidos: [],
         isLoading: false,
         item: this.emptyItem
      };
      this.toggle = this.toggle.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.abrirModalDespachar = this.abrirModalDespachar.bind(this);
      this.pagarPedido = this.pagarPedido.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/Pedido/findByTipoPagoId/1012`)
         .then(res => {
            this.setState({pedidos: res.data, isLoading: false});
         });
   }

   toggle(){
      this.setState({modal: !this.state.modal});
   }

   handleChange(event){
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;

      if(event.target.value >= this.state.item.montoTotal) {
         item.montoCambio = event.target.value - this.state.item.montoTotal ;
			//this.setState({cambio: event.target.value - this.state.montoPagar})
      }
      
      this.setState({item});
   }

   abrirModalDespachar(id){
      axios.get(`${server.ip}/Pedido/findById/${id}`)
			.then(res => {
            console.log(res.data);
				this.setState({
               modal: !this.state.modal,
               item: res.data
					});
			});

   }

   async pagarPedido(){
      const {item} = this.state;
      if(item.montoEntregado >= item.montoTotal){
         this.setState({isLoading: true});
         const datos = {
            pedidoId: item.pedidoId,
            usuarioId: JSON.parse(localStorage.getItem('user')).usuarioId,
            montoEntregado: this.state.item.montoEntregado
         }
         await axios.post(`${server.ip}/Pedido/pagarPedido`, datos)
            .then(res => {
               this.setState({isLoading: false, modal: !this.state.modal});
               this.componentDidMount();
            });
      } else {
         alert('El monto ingresado es menor al que debe cancelar.');
      }
      
   }

   render() {
      const {pedidos, isLoading, item} = this.state;

      if(isLoading){
         return <p>Cargando...</p>;
      }

      const pedidoList = pedidos.map(ped => {
			return <tr key={ped.pedidoId}>
				<td>{ped.numeroPedido}</td>
            <td>{ped.nombreCliente}</td>
            <td>
               <Moment format="DD/MM/YYYY">
                  {ped.fechaRegistro}
               </Moment>
            </td>
				<td>{ped.username}</td>
				<td>{ped.montoTotal}</td>
				<td>
					<ul>
						{ped.detalle.map(d => <li key={d.detallePedidoId}>{d.nombreSubproducto}</li>)}
					</ul>
				</td>
				<td>
					<Button color="success" onClick={() => this.abrirModalDespachar(ped.pedidoId)}>Pagar</Button>
				</td>
			</tr>
      });
      
      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
						<ModalHeader>Cancelar Pedido Pendiente</ModalHeader>
						<ModalBody>
							<div className="row">
								<div className="form-group col-md-8">
									<label>Nombre Cliente</label>
									<input type="text" readOnly name="nombre" value={item.nombreCliente} className="form-control" />
								</div>
							</div>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Monto por Pagar:</label>
									<input type="text" name="montoTotal" readOnly value={item.montoTotal} className="form-control" />
                        </div>
                     </div>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Monto Entregado:</label>
									<input type="text" name="montoEntregado" value={item.montoEntregado} className="form-control"
										onChange={this.handleChange} />
                        </div>
                     </div>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Cambio:</label>
									<input type="text" name="montoCambio" readOnly value={item.montoCambio} className="form-control" />
                        </div>
                     </div>
						</ModalBody>
						<ModalFooter>
							<Button color="primary" className="btn btn-primary" 
									onClick={() => this.pagarPedido()} >Pagar</Button>
                     <Button color="danger" onClick={this.toggle}>Cancelar</Button>
						</ModalFooter>
					</Modal>
               <h3>Listado de Pedidos Pendientes de Pago</h3>
					<Table className="mt-4">
						<thead>
							<tr>
								<th>Número Pedido</th>
                        <th>Cliente</th>
                        <th>Fecha Pedido</th>
								<th>Garzon</th>
								<th>Monto Total</th>
								<th>Detalle</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{pedidoList}
						</tbody>
					</Table>
            </Container>
         </div>
      )
   }
}

export default PedidosPendientes;