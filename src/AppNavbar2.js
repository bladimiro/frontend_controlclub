import React, { Component } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler } from 'reactstrap';
import { Link } from 'react-router-dom';

export default class AppNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {isOpen: false, opciones: [], opcionesMenu: ''};
        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        
        return <Navbar color="primary" dark expand="md">
        <NavbarBrand tag={Link} to="/">Control Club</NavbarBrand>
        <NavbarToggler onClick={this.toggle}/>
        <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            </Nav>
        </Collapse>
    </Navbar>;
    }
}