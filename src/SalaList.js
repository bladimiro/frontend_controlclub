import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';

class SalaList extends Component {
    emptyItem = {
        salaId: '',
        empresaId: 2,
        nombreSala: '',
        turno: ''
    };

   constructor(props) {
		super(props);
		this.state = {salas: [], isLoading: true, item: this.emptyItem};
		this.remove = this.remove.bind(this);

      // metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
	}
	
	componentDidMount() {
		this.setState({isLoading: true});
		fetch('/Sala/findByEmpresaId/2')
			.then(response => response.json())
			.then(data => this.setState({salas: data, isLoading: false}));
	}

	handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
	}
	
	toggle() {
      this.setState({modal: !this.state.modal});
   }

   async guardarDatos(event) {
		event.preventDefault();
		const {item} = this.state;
		
		await fetch('/Sala/insertOrUpdate', {
			method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
		});

		this.setState({item: this.emptyItem});
      this.componentDidMount();
      this.toggle();
	}

	async remove(id) {
		await fetch(`/Sala/delete/${id}`,{
			method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
		}).then(() => {
			let updatedSalas = [...this.state.salas].filter(i => i.salaId !== id);
			this.setState({salas: updatedSalas});
		});
	}

	async editar(id) {
		const sala = await(await fetch(`/Sala/findById/${id}`)).json();
		this.setState({item: sala});
		this.toggle();
	}

	render() {
		const {salas, isLoading} = this.state;
		const {item} = this.state;
		const titulo = <h3>{item.salaId ? 'Editar Sala' : 'Adicionar Sala'}</h3>;

		if(isLoading) {
			return <p>Cargando...</p>;
		}

		const salaList = salas.map(sala => {
			return <tr key={sala.salaId}>
				<td>{sala.nombreSala}</td>
				<td>{sala.turno}</td>
				<td> 
               <ButtonGroup>
                  <Button size="sm" color="primary" onClick={() => this.editar(sala.salaId)}>Editar</Button>
                  <Button size="sm" color="danger" onClick={() => this.remove(sala.salaId)}>Borrar</Button>
               </ButtonGroup>
            </td>
			</tr>
		});

		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<Modal isOpen={this.state.modal}>
						<form onSubmit={this.guardarDatos}>
							<ModalHeader>{titulo}</ModalHeader>
							<ModalBody>
								<div className="row">
                           <div className="form-group col-md-8">
                              <label>Nombre:</label>
                              <input type="text" name="nombreSala" value={item.nombreSala} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
								<div className="row">
                           <div className="form-group col-md-8">
                              <label>Turno:</label>
                              <input type="text" name="turno" value={item.turno} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
							</ModalBody>
							<ModalFooter>
								<input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
							</ModalFooter>
						</form>
					</Modal>
					<h3>Listado de Salas</h3>
					<Button color="success" onClick={this.toggle}>Adicionar</Button>
               <Table>
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Turno</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							{salaList}
						</tbody>
					</Table>
				</Container>
			</div>
		)
	}
}

export default SalaList;