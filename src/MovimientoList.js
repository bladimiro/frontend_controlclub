import React, { Component } from 'react';
import { Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import DatePicker from "react-datepicker";
import axios from 'axios';
import { server } from './servercontext';

import "react-datepicker/dist/react-datepicker.css";
import "react-tabs/style/react-tabs.css";

class MovimientoList extends Component {
   constructor(props) {
      super(props);
      this.state = {
         pedidosPendientes: [], 
         pedidosAtendidos: [], 
         pedidosCancelados: [],
         isLoading: true,
         fechaInicioPendiente: new Date(),
         fechaFinPendiente: new Date(),
         fechaInicioAtendido: new Date(),
         fechaFinAtendido: new Date(),
         fechaInicioCancelado: new Date(),
         fechaFinCancelado: new Date()
      }

      this.handleChangeFecIniPendiente = this.handleChangeFecIniPendiente.bind(this);
      this.handleChangeFecFinPendiente = this.handleChangeFecFinPendiente.bind(this);
      this.handleChangeFecIniAtendido = this.handleChangeFecIniAtendido.bind(this);
      this.handleChangeFecFinAtendido = this.handleChangeFecFinAtendido.bind(this);
      this.handleChangeFecIniCancelado = this.handleChangeFecIniCancelado.bind(this);
      this.handleChangeFecFinCancelado = this.handleChangeFecFinCancelado.bind(this);

      // metodos de busqueda
      this.mostrarPendientes = this.mostrarPendientes.bind(this);
      this.mostrarAtendidos = this.mostrarAtendidos.bind(this);
      this.mostrarCancelados = this.mostrarCancelados.bind(this);
   }

   handleChangeFecIniPendiente(date1){
      this.setState({fechaInicioPendiente: date1});
   }
   handleChangeFecFinPendiente(date1){
      this.setState({fechaFinPendiente: date1});
   }
   handleChangeFecIniAtendido(date1){
      this.setState({fechaInicioAtendido: date1});
   }
   handleChangeFecFinAtendido(date1){
      this.setState({fechaFinAtendido: date1});
   }
   handleChangeFecIniCancelado(date1){
      this.setState({fechaInicioCancelado: date1});
   }
   handleChangeFecFinCancelado(date1){
      this.setState({fechaFinCancelado: date1});
   }

   componentDidMount() {
      this.mostrarPendientes();
      this.mostrarAtendidos();
      this.mostrarCancelados();
   }

   mostrarPendientes() {
      const parametros = {
         id: 1003,
         fechaInicio: this.state.fechaInicioPendiente,
         fechaFin: this.state.fechaFinPendiente
      }
      axios.post(`${server.ip}/Pedido/findByEstadoPedidoIdFechas`, parametros)
         .then(res => {
            this.setState({pedidosPendientes: res.data, isLoading: false});
         });
   }

   mostrarAtendidos() {
      this.setState({isLoading: true});
      const parametros = {
         id: 1004,
         fechaInicio: this.state.fechaInicioAtendido,
         fechaFin: this.state.fechaFinAtendido
      }
      axios.post(`${server.ip}/Pedido/findByEstadoPedidoIdFechas`, parametros)
         .then(res => {
            this.setState({pedidosAtendidos: res.data, isLoading: false});
         });
   }

   mostrarCancelados(){
      this.setState({isLoading: true});
      const parametros = {
         id: 1005,
         fechaInicio: this.state.fechaInicioCancelado,
         fechaFin: this.state.fechaFinCancelado
      }
      axios.post(`${server.ip}/Pedido/findByEstadoPedidoIdFechas`, parametros)
         .then(res => {
            this.setState({pedidosCancelados: res.data, isLoading: false});
         });
   }

   render() {
      const {pedidosPendientes, pedidosAtendidos, pedidosCancelados, isLoading} = this.state;

      if(isLoading) {
         return <p>Cargando</p>;
      }

      const pendientesList = pedidosPendientes.map(pend => {
         return <tr key={pend.pedidoId}>
            <td>{pend.numeroPedido}</td>
				<td>{pend.numeroMesa}</td>
				<td>{pend.montoTotal}</td>
            <td>{pend.montoEntregado}</td>
				<td>
					<ul>
						{pend.detalle.map(d => <li key={d.detallePedidoId}>{d.cantidad} - {d.nombreSubproducto}</li>)}
					</ul>
				</td>
            <td>{pend.username}</td>
         </tr>
      });

      const atendidosList = pedidosAtendidos.map(aten => {
         return <tr key={aten.pedidoId}>
            <td>{aten.numeroPedido}</td>
				<td>{aten.numeroMesa}</td>
				<td>{aten.montoTotal}</td>
            <td>{aten.montoEntregado}</td>
				<td>
					<ul>
						{aten.detalle.map(d => <li key={d.detallePedidoId}>{d.cantidad} - {d.nombreSubproducto}</li>)}
					</ul>
				</td>
            <td>{aten.username}</td>
         </tr>
      });

      const canceladosList = pedidosCancelados.map(canc => {
         return <tr key={canc.pedidoId}>
            <td>{canc.numeroPedido}</td>
				<td>{canc.numeroMesa}</td>
				<td>{canc.montoTotal}</td>
				<td>
					<ul>
						{canc.detalle.map(d => <li key={d.detallePedidoId}>{d.cantidad} - {d.nombreSubproducto}</li>)}
					</ul>
				</td>
            <td>{canc.username}</td>
            <td>{canc.observacion}</td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <h3>Listado de Pedidos</h3>
               <Tabs>
                  <TabList>
                     <Tab>Pendientes</Tab>
                     <Tab>Atentidos</Tab>
                     <Tab>Cancelados</Tab>
                  </TabList>
                  <TabPanel>
                     <form className="form-inline">
                        <div className="form-group">
                              <label>Inicio: </label>
                              <DatePicker
                                    selected={this.state.fechaInicioPendiente}
                                    onChange={this.handleChangeFecIniPendiente}
                                    dateFormat="dd/MM/yyyy"
                                    className="form-control col-md-8"
                                    />
                        </div>
                        <div className="form-group">
                              <label> Fin: </label>
                              <DatePicker
                                    selected={this.state.fechaFinPendiente}
                                    onChange={this.handleChangeFecFinPendiente}
                                    dateFormat="dd/MM/yyyy"
                                    className="form-control col-md-8"
                                    />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={this.mostrarPendientes}>Buscar</button>
                     </form>
                     
                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Número Pedido</th>
                              <th>Número Mesa</th>
                              <th>Monto Total</th>
                              <th>Monto Pagado</th>
                              <th>Detalle</th>
                              <th>Atendido por</th>
                           </tr>
                        </thead>
                        <tbody>
                           {pendientesList}
                        </tbody>
                     </Table>
                  </TabPanel>
                  <TabPanel>
                     <form className="form-inline">
                        <div className="form-group">
                              <label>Inicio: </label>
                              <DatePicker
                                    selected={this.state.fechaInicioAtendido}
                                    onChange={this.handleChangeFecIniAtendido}
                                    dateFormat="dd/MM/yyyy"
                                    className="form-control col-md-8"
                                    />
                        </div>
                        <div className="form-group">
                              <label> Fin: </label>
                              <DatePicker
                                    selected={this.state.fechaFinAtendido}
                                    onChange={this.handleChangeFecFinAtendido}
                                    dateFormat="dd/MM/yyyy"
                                    className="form-control col-md-8"
                                    />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={this.mostrarAtendidos}>Buscar</button>
                     </form>

                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Número Pedido</th>
                              <th>Número Mesa</th>
                              <th>Monto Total</th>
                              <th>Monto Pagado</th>
                              <th>Detalle</th>
                              <th>Atendido por</th>
                           </tr>
                        </thead>
                        <tbody>
                           {atendidosList}
                        </tbody>
                     </Table>
                  </TabPanel>
                  <TabPanel>
                     <form className="form-inline">
                        <div className="form-group">
                              <label>Inicio: </label>
                              <DatePicker
                                    selected={this.state.fechaInicioCancelado}
                                    onChange={this.handleChangeFecIniCancelado}
                                    dateFormat="dd/MM/yyyy"
                                    className="form-control col-md-8"
                                    />
                        </div>
                        <div className="form-group">
                              <label> Fin: </label>
                              <DatePicker
                                    selected={this.state.fechaFinCancelado}
                                    onChange={this.handleChangeFecFinCancelado}
                                    dateFormat="dd/MM/yyyy"
                                    className="form-control col-md-8"
                                    />
                        </div>
                        <button type="button" class="btn btn-primary" onClick={this.mostrarCancelados}>Buscar</button>
                     </form>

                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Número Pedido</th>
                              <th>Número Mesa</th>
                              <th>Monto Total</th>
                              <th>Detalle</th>
                              <th>Atendido por</th>
                              <th>Motivo</th>
                           </tr>
                        </thead>
                        <tbody>
                           {canceladosList}
                        </tbody>
                     </Table>
                  </TabPanel>
               </Tabs>
            </Container>
         </div>
      )
   }
}

export default MovimientoList;   