import React, { Component } from 'react';
import { Container, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import Moment from 'react-moment';
import { server } from './servercontext';

class PedidoList extends Component {

	emptyItem = {
		pedidoId: '',
		clienteId: '',
		montoEntregado: '',
		tipoPagoId: '',
		nroDocumento: '0',
		nombre: 'S/N'
	};
	constructor(props) {
		super(props);
		this.state = {
			pedidos: [], 
			detallesPedido: [],
			isLoading: true,
			modalCancelar: false,
			modalDespachar: false,
			modalImprimir: false,
			motivo: '',
			pedidoId: '',
			monto: 0,
			nroPedido: '',
			montoPagar: 0,
			cambio: 0,
			item: this.emptyItem,
			tipos: [],
			datosImprimir: '',
			tipoPagoId: null,
			clientes: [],
			clienteId: ''
		};

		this.despacharPedido = this.despacharPedido.bind(this);
		this.cancelarPedido = this.cancelarPedido.bind(this);
		this.abrirModalCancelar = this.abrirModalCancelar.bind(this);
		this.cerrarModalCancelar = this.cerrarModalCancelar.bind(this);
		this.handleChangeMotivo = this.handleChangeMotivo.bind(this);
		this.handleChangeMonto = this.handleChangeMonto.bind(this);
		this.abrirModalDespachar = this.abrirModalDespachar.bind(this);
		this.abrirModalImprimir = this.abrirModalImprimir.bind(this);
		this.cerrarModalImprimir = this.cerrarModalImprimir.bind(this);
		this.cerrarModalDespachar = this.cerrarModalDespachar.bind(this);
		this.handleChangeTipoPago = this.handleChangeTipoPago.bind(this);
		this.handleChangeDocu = this.handleChangeDocu.bind(this);
		this.handleBlur = this.handleBlur.bind(this);
		this.imprimir = this.imprimir.bind(this);
	}

	componentDidMount() {
		axios.get(`${server.ip}/Dominio/findByDominio/TipoPagoID`)
			.then(res => {
				this.setState({tipos: res.data});
			});
		this.recuperarDatos();
		axios.get(`${server.ip}/Clientes/findAll`)
			.then(res => {
				this.setState({clientes: res.data});
			});
	}

	handleBlur() {
		if(this.state.item.nroDocumento !== 0){
			axios.get(`${server.ip}/Clientes/findByDocumentoIdentidad/${this.state.item.nroDocumento}`)
				.then(res => {
					let item = {...this.state.item};
					item['nombre'] = res.data.nombre;
					item['clienteId'] = res.data.clienteId;
					this.setState({item});
				})
				.catch(err => {
					alert("No existe usuario");
				});
		}
	}

	imprimir(){
		var content = document.getElementById("printPedido").innerHTML;
      var mywindow = window.open('', 'height=600,width=800');
      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      mywindow.print();
      mywindow.close();
      return true;
	}

	abrirModalImprimir(id){
		const ind = this.state.pedidos.findIndex(p => p.pedidoId === id);
		//const datos = this.state.pedidos[ind];
		this.setState({datosImprimir: this.state.pedidos[ind]});
		console.log(this.state.datosImprimir);
		
		axios.get(`${server.ip}/DetallePedido/findByPedidoId/${id}`)
			.then(res => {
				this.setState({detallesPedido: res.data});
			})
		//console.log(this.state.pedidos[ind]);
		this.setState({modalImprimir: !this.state.modalImprimir});

	}
	cerrarModalImprimir(){
		this.setState({modalImprimir: !this.state.modalImprimir});
	}

	handleChangeDocu(event){
		const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
	}

	recuperarDatos(){
		this.setState({isLoading: true});
		axios.get(`${server.ip}/Pedido/findByEstadoPedidoId/1003`)
			.then(res => {
				this.setState({pedidos: res.data, isLoading: false})
			});
	}

	async despacharPedido(id) {
		//console.log(id);
		if(this.validarDatos()){
			const pedido = {
				pedidoId: this.state.pedidoId,
				usuarioDespachaId: JSON.parse(localStorage.getItem('user')).usuarioId,
				montoEntregado: this.state.monto,
				tipoPagoId: this.state.tipoPagoId,
				clienteId: this.state.item.clienteId
			}
			console.log(pedido);
			axios.post(`${server.ip}/Pedido/despacharPedido`, pedido)
				.then(res => {
					this.recuperarDatos();
					this.setState({modalDespachar: !this.state.modalDespachar});
				});
		}
		
	}

	validarDatos(){
		if(this.state.tipoPagoId === null){
			alert("Tiene que seleccionar el tipo de pago.");
			return false;
		}		
		
		if(this.state.monto < this.state.montoPagar){
			alert("El monto de la cuenta es mayor al monto ingresado.");
			return false;
		}

		return true;
	}

	async cancelarPedido(){
		const pedido = {
			pedidoId: this.state.pedidoId,
			usuarioCancelaId: JSON.parse(localStorage.getItem('user')).usuarioId,
			observacion: this.state.motivo
		}
		axios.post(`${server.ip}/Pedido/cancelarPedido`, pedido)
			.then(res => {
				this.recuperarDatos();
				this.setState({modalCancelar: !this.state.modalCancelar});
			});
	}

	async abrirModalDespachar(id){
		// 
		axios.get(`${server.ip}/Pedido/findById/${id}`)
			.then(res => {
				this.setState({
					modalDespachar: !this.state.modalDespachar, 
					monto: 0, 
					cambio: 0, 
					pedidoId: id, 
					nroPedido: res.data.numeroPedido, 
					montoPagar: res.data.montoTotal});
			});
		
	}
	cerrarModalDespachar(){
		this.setState({modalDespachar: !this.state.modalDespachar, monto: 0});
	}
	abrirModalCancelar(id){
		this.setState({modalCancelar: !this.state.modalCancelar, motivo: '', pedidoId: id});
	}
	cerrarModalCancelar(){
		this.setState({modalCancelar: !this.state.modalCancelar, motivo: ''});
	}
	handleChangeMotivo(event){
		this.setState({motivo: event.target.value});
	}
	handleChangeTipoPago(event){
		console.log(event.target.value);
		this.setState({tipoPagoId: event.target.value});
	}
	handleChangeMonto(event){
		this.setState({monto: event.target.value});
		
		if(event.target.value >= this.state.montoPagar) {
			this.setState({cambio: event.target.value - this.state.montoPagar})
		}
	}

	render() {
		const {pedidos, clientes, detallesPedido, tipos, isLoading, motivo, monto, montoPagar, cambio, tipoPagoId, item, datosImprimir} = this.state;

		if(isLoading) {
			return <p>Cargando...</p>;
		}

		//console.log(detallesPedido);
		const detalleList = detallesPedido.map(det => {
			return <tr key={det.detallePedidoId}>
				<td width="20%" align="center">{det.cantidad}</td>
				<td width="60%">
				{det.nombreSubproducto}
				{det.damasConcatenadas}
				</td>
				<td width="20%" align="right">{det.precioUnitario}</td>
			</tr>
		});

		const tipoList = tipos.map(ti => {
			return <option value={ti.dominioId}>{ti.descripcion}</option>
		});
		
		const clienteList = clientes.map(cli => {
			return <option value={cli.clienteId}>{cli.nombre}</option>
		});

		const pedidoList = pedidos.map(ped => {
			return <tr key={ped.pedidoId}>
				<td>{ped.numeroPedido}</td>
				<td>{ped.username}</td>
				<td align="right">{ped.montoTotal}</td>
				<td>
					<ul>
						{ped.detalle.map(d => <li key={d.detallePedidoId}>{d.cantidad} - {d.nombreSubproducto} {d.damasConcatenadas} {d.precioUnitario} </li>)}
					</ul>
				</td>
				<td>
					<Button color="default" onClick={() => this.abrirModalImprimir(ped.pedidoId)}>Imprimir</Button>
					<Button color="success" onClick={() => this.abrirModalDespachar(ped.pedidoId)}>Despachar</Button>
					<Button color="warning" onClick={() => this.abrirModalCancelar(ped.pedidoId)}>Cancelar</Button>
				</td>
			</tr>
		});

		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<Modal isOpen={this.state.modalImprimir}>
						<ModalHeader>Pedido</ModalHeader>
						<ModalBody>
							<div id="printPedido">
								<div>
									<table width="250" cellPadding="0" cellPadding="0">
										<tr>
											<td><b>Garzon: </b>{datosImprimir.username}</td>
											<td><b>Pedido: </b>{datosImprimir.numeroPedido}</td>
										</tr>
										<tr>
											<td><b>Fecha: </b><Moment format="DD/MM/YYYY">{datosImprimir.fechaRegistro}</Moment></td>
											<td><b>Hora: </b><Moment format="HH:MM">{datosImprimir.fechaRegistro}</Moment></td>
										</tr>
									</table>
								</div>
								<div>
									<table width="250">
										<thead>
											<tr>
												<td><b>Cant.</b></td>
												<td><b>Producto</b></td>
												<td><b>Precio</b></td>
											</tr>
										</thead>
										<tbody>
											{detalleList
											}
											<tr>
												<td></td>
												<td><b>Total:</b> </td>
												<td align="right"><b>{datosImprimir.montoTotal}</b></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</ModalBody>
						<ModalFooter>
							<Button color="primary" className="btn btn-primary" 
									onClick={() => this.imprimir()} >Imprimir</Button>
                     <Button color="danger" onClick={this.cerrarModalImprimir}>Cerrar</Button>
						</ModalFooter>
					</Modal>
					<Modal isOpen={this.state.modalDespachar}>
						<ModalHeader>Despachar Pedido</ModalHeader>
						<ModalBody>
							<div className="row">
								<div className="form-group col-md-8">
									<label>Tipo Pago:</label>
									<select name="tipoPagoId" value={tipoPagoId} onChange={this.handleChangeTipoPago}
									 className="form-control">
										<option value=''></option>
										{tipoList}
									</select>
								</div>
							</div>
							<div className="row">
								<div className="form-group col-md-8">
									<label>Nombre Cliente</label>
									<select className='form-control'>
										<option value=''></option>
										{clienteList}
									</select>
								</div>
							</div>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Monto por Pagar:</label>
									<input type="text" name="montoPagar" readOnly value={montoPagar} className="form-control" />
                        </div>
                     </div>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Monto Entregado:</label>
									<input type="number" name="monto" value={monto} className="form-control"
										onChange={this.handleChangeMonto} />
                        </div>
                     </div>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Cambio:</label>
									<input type="text" name="cambio" readOnly value={cambio} className="form-control" />
                        </div>
                     </div>
						</ModalBody>
						<ModalFooter>
							<Button color="primary" className="btn btn-primary" 
									onClick={() => this.despacharPedido()} >Despachar</Button>
                     <Button color="danger" onClick={this.cerrarModalDespachar}>Cancelar</Button>
						</ModalFooter>
					</Modal>
					<Modal isOpen={this.state.modalCancelar}>
						<ModalHeader>Cancelar Pedido</ModalHeader>
						<ModalBody>
							<div className="row">
                        <div className="form-group col-md-8">
                     		<label>Motivo:</label>
									<textarea name="motivo" value={motivo} className="form-control" cols="3"
										onChange={this.handleChangeMotivo} />
                        </div>
                     </div>
						</ModalBody>
						<ModalFooter>
							<Button color="primary" className="btn btn-primary" 
									onClick={() => this.cancelarPedido()} >Cancelar</Button>
                     <Button color="danger" onClick={this.cerrarModalCancelar}>Cerrar</Button>
						</ModalFooter>
					</Modal>
					<h3>Listado de Pedidos Pendientes</h3>
					<Button color="success" onClick={this.recuperarDatos}>Refresh</Button>
					<Table className="mt-4">
						<thead>
							<tr>
								<th>Número Pedido</th>
								<th>Atendido</th>
								<th>Monto Total</th>
								<th>Detalle</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{pedidoList}
						</tbody>
					</Table>
				</Container>
			</div>
		)
	}
}

export default PedidoList;