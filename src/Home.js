import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import { Container } from 'reactstrap';
import logo from './controlclub.png';
//
class Home extends Component {
    componentDidMount() {
        if(localStorage.getItem('user') === null){
            this.props.history.push("/login");
        } else {
            console.log("no nulo");
        }
    }
    render() {
        return (
            <div>
                <AppNavbar/>              
                <Container fluid>
                <br/>
                    <div className="jumbotron col-12">
                        <h2>Bienvenidos a </h2>
                        <img src={logo} />
                    </div>
                </Container>
            </div>
        );
    }
}

export default Home;