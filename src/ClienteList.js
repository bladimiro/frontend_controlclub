import React, { Component } from 'react';
import { Button, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import { server } from './servercontext';

class ClienteList extends Component {
	emptyItem = {
		clienteId: '',
		nombre: '',
		nit: ''
	};

	constructor(props) {
		super(props);
		this.state = {clientes: [], isLoading: true, item: this.emptyItem};
		this.remove = this.remove.bind(this);

		// metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		this.setState({isLoading: true});
		axios.get(`${server.ip}/Clientes/findAll`)
			.then(res => {
				this.setState({clientes: res.data, isLoading: false});
			})
		/*fetch('/Clientes/findAll')
			.then(response => response.json())
			.then(data => this.setState({clientes: data, isLoading: false}));*/
	}

	handleChange(event) {
		const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
	}

	toggle() {
      this.setState({modal: !this.state.modal});
	}

	async guardarDatos(event) {
		event.preventDefault();
		const {item} = this.state;

		await axios.post(`${server.ip}/Clientes/insertOrUpdate`, item)
			.then(res => {
				console.log(res);
			})
		/*await fetch('/Clientes/insertOrUpdate',{
			method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
		});*/

		this.setState({item: this.emptyItem});
		this.componentDidMount();
		this.toggle();
	}

	async remove(id) {
		await axios.get(`${server.ip}/Clientes/delete/${id}`)
			.then(res => {
				let updatedClientes = [...this.state.clientes].filter(i => i.clienteId !== id);
				this.setState({clientes: updatedClientes});	
			})
		/*await fetch(`/Clientes/delete/${id}`,{
			method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
		}).then(() => {
			let updatedClientes = [...this.state.clientes].filter(i => i.clienteId !== id);
			this.setState({clientes: updatedClientes});
		});*/
	}

	async editar(id) {
		//const cliente = await(await fetch(`/Clientes/findById/${id}`)).json();
		axios.get(`${server.ip}/Clientes/findById/${id}`)
			.then(res => {
				this.setState({item: res.data});
				this.toggle();
			});
		
	}

	render() {
		const {clientes, isLoading, item} = this.state;
		const titulo = <h3>{item.clienteId ? 'Editar Cliente' : 'Adicionar Cliente'}</h3>;

		if(isLoading) {
			return <p>Cargando...</p>;
		}

		const clienteList = clientes.map(clie => {
			return <tr key={clie.clienteId}>
				<td>{clie.nombre}</td>
				<td>{clie.nit}</td>
				<td>
					<Button size="sm" color="primary" onClick={() => this.editar(clie.clienteId)}>Editar</Button>
				</td>
			</tr>
		});

		return (
			<div>
				<AppNavbar/>
            <Container fluid>
					<Modal isOpen={this.state.modal}>
						<form onSubmit={this.guardarDatos}>
							<ModalHeader>{titulo}</ModalHeader>
							<ModalBody>
								<div className="row">
									<div className="form-group col-md-8">
										<label>Nombre:</label>
										<input type="text" name="nombre" value={item.nombre} className="form-control"
												onChange={this.handleChange} />
									</div>
								</div>
								<div className="row">
									<div className="form-group col-md-8">
										<label>NIT/CI:</label>
										<input type="text" name="nit" value={item.nit} className="form-control"
												onChange={this.handleChange} />
									</div>
								</div>
							</ModalBody>
							<ModalFooter>
								<input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
							</ModalFooter>
						</form>
					</Modal>
					<h3>Listado de Clientes</h3>
					<Button color="success" onClick={this.toggle}>Adicionar</Button>
					<Table className="mt-4">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>NIT/CI</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{clienteList}
						</tbody>
					</Table>
				</Container>
			</div>
		)
	}
}

export default ClienteList;