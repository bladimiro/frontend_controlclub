import React, { Component } from 'react';
import { Container, Table, Button } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { confirmAlert } from 'react-confirm-alert';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-confirm-alert/src/react-confirm-alert.css';
import axios from 'axios';
import { server } from './servercontext';
import Moment from 'react-moment';

class CierreComandas extends Component {
   emptyItem = {
      comandaMeseroId: '',
      fechaRegistro: '',
      usuarioId: ''
   };

   constructor(props) {
      super(props);
      this.state = {
         inicioComanda: '',
         historicos: [],
         pedidos: [],
         item: this.emptyItem,
         isLoading: false,
         existeComanda: false
      };

      this.cerrarRegistro = this.cerrarRegistro.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/ComandasMeseros/findAbiertoByUsuarioId/${JSON.parse(localStorage.getItem('user')).usuarioId}`)
         .then(res => {
            //console.log(res.data);
            this.setState({item: res.data});

            axios.get(`${server.ip}/DetalleComandas/findByComandaMeseroId/${this.state.item.comandaMeseroId}`)
               .then(res => {
                  console.log(res.data);
                  this.setState({pedidos: res.data});
               });
         });

      axios.get(`${server.ip}/ComandasMeseros/findCerradoByEstadoRegistroId/${JSON.parse(localStorage.getItem('user')).usuarioId}/1007`)
         .then(res => {
            this.setState({historicos: res.data, isLoading: false});
         });
   }

   mostrarTipoPago(tipo){
      if(tipo === 1011){
         return "AL CONTADO";
      }
      if(tipo === 1012){
         return "A CREDITO";
      }
   }

   mostrarEstadoPedido(estado){
      if(estado === 1003){
         return "SOLICITADO";
      }
      if(estado === 1004){
         return "ATENDIDO";
      }
      if(estado === 1005){
         return "CANCELADO";
      }
   }

   cerrarRegistro(){
      confirmAlert({
         title: 'Cerrar Comandas',
         message: 'Confirma cerrar comandas?',
         buttons: [
            {
               label: 'Si',
               onClick: () => {
                  axios.get(`${server.ip}/ComandasMeseros/cerrarComandas/${JSON.parse(localStorage.getItem('user')).usuarioId}`)
                     .then(res => {
                        console.log(res);
                     });
               }
            },
            {
               label: 'No',
               onClick: () => {}
            }
         ]
      })
   }

   render() {
      const {item, isLoading, historicos, pedidos, existeComanda} = this.state;

      if(isLoading){
         return <p>Cargando...</p>;
      }

      const pedidoList = pedidos.map(ped => {
         return <tr key={ped.detalleComandaId}>
            <td>{ped.nroPedido}</td>
            <td>
               <Moment format="DD/MM/YYYYY HH:MM">
                  {ped.fechaRegistro}
               </Moment>
            </td>
            <td>{this.mostrarEstadoPedido(ped.estadoPedidoId)}</td>
            <td>{this.mostrarTipoPago(ped.tipoPagoId)}</td>
            <td>
               <ul>
                  {ped.datosPedido.map(d => <li key={d.detallePedidoId}>{d.cantidad} - {d.nombreSubproducto}{d.damasConcatenadas}</li>)}
               </ul>
            </td>
         </tr>
      });

      const historicoList = historicos.map(his => {
         return <tr key={his.comandaMeseroId}>
            <td>
               {his.comandaMeseroId}
            </td>
            <td>
               <Moment format="DD/MM/YYYY HH:MM">
                  {his.fechaRegistro}
               </Moment>
            </td>
            <td>
               <Moment format="DD/MM/YYYY HH:MM">
                  {his.fechaCierre}
               </Moment>
            </td>
            <td> </td>
         </tr>
      });

      //const 

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Tabs>
                  <TabList>
                     <Tab>Abierto</Tab>
                     <Tab>Cerrados</Tab>
                  </TabList>
                  <TabPanel>
                     <form className="form-inline">
                        
                        <div className="form-group">
                           <label>Inicio: </label>
                           { existeComanda ? <Moment format="DD/MM/YYYY HH:MM"> item.fechaRegistro </Moment>: null}
                        </div>
                        { existeComanda ? <button type="button" class="btn btn-success" onClick={this.cerrarRegistro}>Cerrar</button> : null}
                     </form> 
                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Numero Pedido</th>
                              <th>Fecha y Hora Pedido</th>
                              <th>Estado Pedido</th>
                              <th>Pago</th>
                              <th>Detalle </th>
                           </tr>
                        </thead>
                        <tbody>
                           {pedidoList}
                        </tbody>
                     </Table>
                  </TabPanel>
                  <TabPanel>
                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Numero</th>
                              <th>Inicio</th>
                              <th>Cierre</th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>
                           {historicoList}
                        </tbody>
                     </Table>
                  </TabPanel>
               </Tabs>
               
            </Container>
         </div>
      )
   }
}

export default CierreComandas;