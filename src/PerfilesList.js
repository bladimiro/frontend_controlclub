import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { server } from './servercontext';

class PerfilesList extends Component {
   emptyItem = {
      perfilUsuarioId: '',
      usuarioId: '',
      perfilId: ''
   };

   constructor(props) {
      super(props);
      this.state = {
         perfilesUsuario: [],
         perfiles: [],
         item: this.emptyItem,
         perfilUsuarioId: '',
         isLoading: false,
         usuarioId: 0
      };
      this.remove = this.remove.bind(this);
      // metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.nuevoPerfil = this.nuevoPerfil.bind(this);
   }

   componentDidMount() {
      const id = this.props.match.params.id;
      this.setState({usuarioId: id});
      //this.desplegarPerfiles();
      axios.get(`${server.ip}/Perfiles/findAll`)
         .then(res => {
            this.setState({perfiles: res.data});
         });

      axios.get(`${server.ip}/PerfilesUsuarios/findPerfilesByUsuarioId/${id}`)
         .then(res => {
            this.setState({perfilesUsuario: res.data, isLoading: false});
         });
   }

   desplegarPerfiles(){
      const usuarioId = {...this.state.usuarioId};
      ///console.log("ID " + usuarioId);
      this.setState({isLoading: true});
      
      /*fetch(`/PerfilesUsuarios/findPerfilesByUsuarioId/${id}`)
         .then(response => response.json())
         .then(data => this.setState({perfilesUsuario: data, isLoading: false}));*/
   }

   handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }

   toggle() {
      this.setState({modal: !this.state.modal});
   }

   async guardarDatos(event) {
      event.preventDefault();
      const {item} = this.state;
      item.usuarioId = this.state.usuarioId;

      await axios.post(`${server.ip}/PerfilesUsuarios/insertOrUpdate`, item)
         .then(res => {
            this.setState({item: this.emptyItem});
            this.componentDidMount();
         });
      
      this.toggle();
   }

   nuevoPerfil(event){
      this.setState({item: this.emptyItem});
      this.toggle();
   }

   async remove(id) {
      await axios.get(`${server.ip}/PerfilesUsuarios/delete/${id}`)
         .then(res => {
            let updatedPerfilesUsuarios = [...this.state.perfilesUsuario].filter(i => i.perfilUsuarioId !== id);
            this.setState({perfilesUsuario: updatedPerfilesUsuarios});
         });
   }

   async editar(id) {
      await axios.get(`${server.ip}/PerfilesUsuarios/findById/${id}`)
         .then(res => {
            this.setState({item: res.data});
            this.toggle();
         });
   }

   render() {
      const {item, perfilesUsuario, isLoading, perfiles} = this.state;
      const titulo = <h3>{item.perfilUsuarioId ? 'Editar Perfil' : 'Adicionar Perfil'}</h3>;
      console.log(perfilesUsuario);

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      console.log(perfilesUsuario);
      const perfilUsuarioList = perfilesUsuario.map(perf => {
         return <tr key={perf.perfilUsuarioId}>
            <td>{perf.perfNombre}</td>
            <td> 
               <ButtonGroup>
                  <Button size="sm" color="primary" onClick={() => this.editar(perf.perfilUsuarioId)}>Editar</Button>
                  <Button size="sm" color="danger" onClick={() => this.remove(perf.perfilUsuarioId)}>Borrar</Button>
               </ButtonGroup>
            </td>
         </tr>
      });

      const perfilList = perfiles.map(per => {
         return <option value={per.perfilId}>{per.nombre}</option>
      })

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarDatos}>
                     <ModalHeader>{titulo}</ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Perfil:</label>
                              <select name="perfilId" value={item.perfilId} onChange={this.handleChange} className="form-control">
                                 <option value=''></option>
                                 {perfilList}
                              </select>
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Perfiles Asignados</h3>
               <Button color="success" onClick={this.nuevoPerfil}>Adicionar</Button>
               <Table className="mt-4">
                  <thead>
                     <tr>
                        <th>Perfil</th>
                        <th>Acciones</th>
                     </tr>
                  </thead>
                  <tbody>
                     {perfilUsuarioList}
                  </tbody>
               </Table>
               <Link color="success" to="/usuarios">Volver</Link>
            </Container>
         </div>
      )
   }
}

export default PerfilesList;