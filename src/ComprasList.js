import React, { Component } from 'react';
import { Button, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import DatePicker from "react-datepicker";
import Moment from 'react-moment';
import axios from 'axios';
import { server } from './servercontext';

class ComprasList extends Component {
   emptyItem = {
      provProductoId: '',
      cantidad: 0,
      fechaCompra: '',
      precio: 0,
      productoId: '',
      proveedorId: ''
   };

   constructor(props) {
      super(props);
      this.state = {
         compras: [],
         productos: [],
         proveedores: [],
         productoId: '',
         proveedorId: '',
         isLoading: true,
         item: this.emptyItem,
         fechaCompra: new Date(),
         modal: false
      };

      //
      this.toggle = this.toggle.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleChangeFechaCompra = this.handleChangeFechaCompra.bind(this);
      this.nuevaCompra = this.nuevaCompra.bind(this);
      this.guardarCompra = this.guardarCompra.bind(this);
      this.handleChangeProveedor = this.handleChangeProveedor.bind(this);
      this.handleChangeProducto = this.handleChangeProducto.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/ProveedoresProducto/findByActives`)
         .then(res => {
            this.setState({compras: res.data});
         });
      axios.get(`${server.ip}/Productos/findAll`)
         .then(res => {
            this.setState({productos: res.data, isLoading: false});
         });
      axios.get(`${server.ip}/Proveedores/findAll`)
         .then(res => {
            this.setState({proveedores: res.data});
         });
      /*fetch('/ProveedoresProducto/findByActives')
         .then(response => response.json())
         .then(data => this.setState({compras: data}));
      fetch('/Productos/findAll')
         .then(response => response.json())
         .then(data => this.setState({productos: data, isLoading: false}));
      fetch('/Proveedores/findAll')
         .then(response => response.json())
         .then(data => this.setState({proveedores: data}));*/
   }

   handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }

   handleChangeFechaCompra(date1){
      this.setState({fechaCompra: date1});
   }

   handleChangeProveedor(event){
      this.setState({proveedorId: event.target.value});
   }

   handleChangeProducto(event) {
      this.setState({productoId: event.target.value});
   }

   toggle() {
      this.setState({modal: !this.state.modal });
   }

   nuevaCompra(){
      this.setState({item: this.emptyItem});
      this.toggle();
   }

   async guardarCompra(event){
      //alert(JSON.parse(localStorage.getItem('user')).usuarioId);
      const {item} = this.state;
      console.log(item);
      event.preventDefault();

      item.usuarioId = JSON.parse(localStorage.getItem('user')).usuarioId;
      item.productoId = this.state.productoId;
      item.proveedorId = this.state.proveedorId;
      item.fechaCompra = this.state.fechaCompra;
      console.log(item);

      await axios.post(`${server.ip}/ProveedoresProducto/insertOrUpdate`, item)
         .then(res => {
            this.setState({item: this.emptyItem});
            this.toggle();
         });
      /*await fetch('/ProveedoresProducto/insertOrUpdate', {
         method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
      });*/
      
   }

   render() {
      const {productos, proveedores, item, compras, isLoading, productoId, proveedorId} = this.state;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const productoList = productos.map(prod => {
         return <option value={prod.productoId}>{prod.nombre}</option>
      });

      const proveedorList = proveedores.map(prov => {
         return <option value={prov.proveedorId}>{prov.nombre}</option>
      });

      const comprasList = compras.map(com => {
         return <tr key={com.provProductoId}>
            <td>{com.proveedorNombre}</td>
            <td>{com.productoNombre}</td>
            <td>
               <Moment format="DD/MM/YYYY">
                  {com.fechaCompra}
               </Moment>
            </td>
            <td>{com.cantidad}</td>
            <td>{com.precio}</td>
            <td></td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarCompra}>
                     <ModalHeader>Registrar Compra</ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Proveedor:</label>
                              <select name="proveedorId" value={proveedorId} onChange={this.handleChangeProveedor} className="form-control">
											<option value=''></option>
											{proveedorList}
										</select>
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Producto:</label>
                              <select name="productoId" value={productoId} onChange={this.handleChangeProducto} className="form-control">
											<option value=''></option>
											{productoList}
										</select>
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Fecha Compra:</label>
                              <DatePicker
                                 selected={this.state.fechaCompra}
                                 onChange={this.handleChangeFechaCompra}
                                 dateFormat="dd/MM/yyyy"
                                 className="form-control col-md-8"
                                 />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Cantidad Botellas:</label>
                              <input type="number" name="cantidad" value={item.cantidad} className="form-control"
                                 onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Precio:</label>
                              <input type="number" name="precio" value={item.precio} className="form-control"
                                 onChange={this.handleChange} />
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Comprar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Compras</h3>
               <Button color="success" onClick={this.toggle}>Adicionar</Button>
               <Table className="mt-4">
                  <thead>
                     <tr>
                        <th>Proveedor</th>
                        <th>Producto</th>
                        <th>Fecha Compra</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     {comprasList}
                  </tbody>
               </Table>
            </Container>
         </div>
      );
   }
}

export default ComprasList;