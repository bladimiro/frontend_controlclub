import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { server } from './servercontext';

class SubProductoList extends Component {
   emptyItem = {
      subproductoId: '',
      productoId: '',
      nombre: '',
      tipoId: '',
      equivalencia: 0,
      precio: 0,
      cantidadChicas: 0,
      tipoId: '',
      tipoSolicitudId: '',
      comision: 0
   };

   constructor(props) {
      super(props);
      this.state = {
         subproductos: [], 
         isLoading: true, 
         item: this.emptyItem,
         productoId: '',
         tipos: [],
         tipoId: '',
         tiposSolicitudes: [],
         tipoSolicitudId: ''
      };
      this.remove = this.remove.bind(this);

      // metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleChangeTipo = this.handleChangeTipo.bind(this);
      this.handleChangeTipoSolicitud = this.handleChangeTipoSolicitud.bind(this);
   }

   componentDidMount() {
      const id = this.props.match.params.id;
      this.setState({isLoading: true, productoId: id});
      axios.get(`${server.ip}/Subproducto/findByProductoId/${id}`)
         .then(res => {
            this.setState({subproductos: res.data, isLoading: false});
         });

      axios.get(`${server.ip}/Dominio/findByDominio/TipoSubproductoID`)
         .then(res => {
            this.setState({tipos: res.data});
         });

      axios.get(`${server.ip}/Dominio/findByDominio/TipoSolicitudID`)
         .then(res => {
            this.setState({tiposSolicitudes: res.data});
         });
   }

   handleChangeTipo(event){
      this.setState({tipoId: event.target.value});
   }

   handleChangeTipoSolicitud(event){
      this.setState({tipoSolicitudId: event.target.value});
   }

   handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }

   toggle() {
      this.setState({modal: !this.state.modal});
   }

   mostrarTipo(tipo){
      if(tipo === 1009) {
         return "BOTELLA";
      }
      if(tipo === 1010) {
         return "VASO";
      } else {
         return tipo;
      }
   }

   mostrarTipoSolicitante(tipoSolicitud){
      if(tipoSolicitud === 1013) {
         return "CLIENTE";
      }
      if(tipoSolicitud === 1014) {
         return "CHICA";
      } else {
         return tipoSolicitud;
      }
   }

   async guardarDatos(event) {
      event.preventDefault();
      const {item} = this.state;
      item.productoId = this.state.productoId;
      item.tipoId = this.state.tipoId;
      item.tipoSolicitudId = this.state.tipoSolicitudId;

      await axios.post(`${server.ip}/Subproducto/insertOrUpdate`, item)
         .then(res => {
            this.setState({item: this.emptyItem});
         });
      
      this.componentDidMount();
      this.toggle();
   }

   async remove(id) {
      await axios.get(`${server.ip}/Subproducto/delete/${id}`)
         .then(res => {
            let updatedSubproductos = [...this.state.subproductos].filter(i => i.subproductoId !== id);
            this.setState({subproductos: updatedSubproductos});
         });
   }

   async editar(id) {
      await axios.get(`${server.ip}/Subproducto/findById/${id}`)
         .then(res => {
            this.setState({item: res.data, tipoId: res.data.tipoId, tipoSolicitudId: res.data.tipoSolicitudId});
         });
      this.toggle();
   }

   render() {
      const {subproductos, isLoading, item, tipos, tipoId, tiposSolicitudes, tipoSolicitudId} = this.state;
      
      const titulo = <h3>{item.subproductoId ? 'Editar Subproducto' : 'Adicionar Subproducto'}</h3>;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const tipoList = tipos.map(tip => {
         return <option value={tip.dominioId}>{tip.descripcion}</option>
      });

      const tipoSolicitudList = tiposSolicitudes.map(tso => {
         return <option value={tso.dominioId}>{tso.descripcion}</option>
      });

      const subproductoList = subproductos.map(sprod => {
         return <tr key={sprod.subproductoId}>
            <td>{sprod.nombre}</td>
            <td>{sprod.precio}</td>
            <td>{this.mostrarTipo(sprod.tipoId)}</td>
            <td>{this.mostrarTipoSolicitante(sprod.tipoSolicitudId)}</td>
            <td>{sprod.equivalencia}</td>
            <td>{sprod.cantidadChicas}</td>
            <td>{sprod.comision}</td>
            <td> 
               <ButtonGroup>
                  <Button size="sm" color="primary" onClick={() => this.editar(sprod.subproductoId)}>Editar</Button>
                  <Button size="sm" color="danger" onClick={(e) => { if (window.confirm('Confirma eliminar el Sub-Producto?')) this.remove(sprod.subproductoId) } }>
                     Eliminar
                  </Button>
               </ButtonGroup>
            </td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarDatos}>
                     <ModalHeader>{titulo}</ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Nombre:</label>
                              <input type="text" name="nombre" value={item.nombre} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Tipo:</label>
                              <select name="tipoId" value={tipoId} onChange={this.handleChangeTipo} className="form-control">
											<option value=''></option>
											{tipoList}
										</select>
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Tipo Solicitante:</label>
                              <select name="tipoSolicitudId" value={tipoSolicitudId} className="form-control"
                                    onChange={this.handleChangeTipoSolicitud}>
                                 <option value=''></option>
                                 {tipoSolicitudList}
                              </select>
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Precio:</label>
                              <input type="number" name="precio" value={item.precio} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Cantidad usada de una botella:</label>
                              <input type="number" name="equivalencia" value={item.equivalencia} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Cantidad permitida de chicas:</label>
                              <input type="number" name="cantidadChicas" value={item.cantidadChicas} className="form-control"
                                    onChange={this.handleChange}/>
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Comisión:</label>
                              <input type="number" name="comision" value={item.comision} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Subproductos</h3>
               <Button color="success" onClick={this.toggle}>Adicionar</Button>
               <Table>
                  <thead>
                     <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Tipo</th>
                        <th>Tipo Solicitante</th>
                        <th>Cantidad/Botella</th>
                        <th>Cantidad/Chicas</th>
                        <th>Comisión</th>
                        <th>Acciones</th>
                     </tr>
                  </thead>
                  <tbody>
                     {subproductoList}
                  </tbody>
               </Table>
               <Link color="success" to="/productos">Volver</Link>
            </Container>
         </div>
      );
   }
}

export default SubProductoList;