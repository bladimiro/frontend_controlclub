import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { server } from './servercontext';

class ProductoList extends Component {

    emptyItem = {
        productoId: '',
        nombre: '',
        precio: 0,
        cantidadVasos: 0,
        usuarioId: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            productos: [], 
            isLoading: true, 
            item: this.emptyItem,
            itemCompra: this.emptyItemCompra,
            modalCompra: false,
            productoId: '',
            proveedores: [],
            proveedorId: '',
            fechaCompra: new Date()
        };
        this.remove = this.remove.bind(this);

        // metodos modal
        this.toggle = this.toggle.bind(this);
        this.guardarDatos = this.guardarDatos.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.setState({isLoading: true});
        axios.get(`${server.ip}/Productos/findAll`)
            .then(res => {
                this.setState({productos: res.data, isLoading: false});
            });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

    handleChangeFechaCompra(date1){
        this.setState({fechaCompra: date1});
    }

    handleChangeProveedor(event){
        this.setState({proveedorId: event.target.value});
    }

    toggleComprar() {
        this.setState({modalCompra: !this.state.modalCompra});
    }

    nuevaCompra(id) {
        this.setState({productoId: id});
        this.toggleComprar();
    }

    toggle() {
        this.setState({modal: !this.state.modal });
    }

    async guardarDatos(event) {
        event.preventDefault();
        const {item} = this.state;
        item.usuarioId = JSON.parse(localStorage.getItem('user')).usuarioId;

        await axios.post(`${server.ip}/Productos/insertOrUpdate`, item)
            .then(res => {

            });
        
        this.setState({item: this.emptyItem});
        this.componentDidMount();
        this.toggle();
    }

    async remove(id) {
        await axios.get(`${server.ip}/Productos/delete/${id}`)
            .then(res => {
                let updatedProductos = [...this.state.productos].filter(i => i.productoId !== id);
                this.setState({productos: updatedProductos});    
            });
    }

    async nuevo() {
        this.toggle();
    }

    async editar(id) {
        await axios(`${server.ip}/Productos/findById/${id}`)
            .then(res => {
                this.setState({item: res.data});
            });
        
        //this.setState({item: producto});
        this.toggle();
    }

    render() {
        const {productos, isLoading} = this.state;
        const {item} = this.state;
        const titulo = <h3>{item.productoId ? 'Editar Producto' : 'Adicionar Producto'}</h3>

        if(isLoading) {
            return <p>Cargando...</p>;
        }

        const productoList = productos.map(prod => {
            return <tr key={prod.productoId}>
                <td>{prod.nombre}</td>
                <td> 
                    <ButtonGroup>
                        <Button size="sm" color="primary" onClick={() => this.editar(prod.productoId)}>Editar</Button>
                        <Button size="sm" color="danger" onClick={(e) => { if (window.confirm('Confirma eliminar el Producto?')) this.remove(prod.productoId) } }>
                            Eliminar
                        </Button>
                        <Button color="success" tag={Link} to={"/subproductos/" + prod.productoId}>Detalle</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <Modal isOpen={this.state.modal}>
                        <form onSubmit={this.guardarDatos}>
                            <ModalHeader>{titulo}</ModalHeader>
                            <ModalBody>
                                <div className="row">
                                    <div className="form-group col-md-8">
                                        <label>Nombre:</label>
                                        <input type="text" name="nombre" value={item.nombre} className="form-control"
                                            onChange={this.handleChange} />
                                    </div>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                                <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                                
                            </ModalFooter>
                        </form>
                    </Modal>
                    <h3>Listado de Productos</h3>
                    <Button color="success" onClick={this.toggle}>Adicionar</Button>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {productoList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ProductoList;