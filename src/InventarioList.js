import React, {Component} from 'react';
import { Container, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import Moment from 'react-moment';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import "./printStyle.css";
import axios from 'axios';
import { server } from './servercontext';

class InventarioList extends Component {
   emptyItem = {
      inventarioId: '',
      fechaInicio: ''
   };
   constructor(props) {
      super(props);
      this.state = {
         inventarios: [], 
         isLoading: true,
         item: this.emptyItem,
         detalles: [],
         detalleHistorico: [],
         historico: '',
         modal: false
      };
      this.toggle = this.toggle.bind(this);
      this.mostrarEstado = this.mostrarEstado.bind(this);
      this.cerrarInventario = this.cerrarInventario.bind(this);
      this.mostrarDatosHistorico = this.mostrarDatosHistorico.bind(this);
      this.imprimir = this.imprimir.bind(this);
   }

   componentDidMount() {
      this.mostrarDatos();
   }

   mostrarDatos(){
      this.setState({isLoading: true});
      axios.get(`${server.ip}/Inventarios/findByEstadoHistorico`)
         .then(res => {
            this.setState({inventarios: res.data, isLoading: false})
         });
      axios.get(`${server.ip}/Inventarios/verificarRegistrarInventarioAbierto/${JSON.parse(localStorage.getItem('user')).usuarioId}`)
         .then(res => {
            this.setState({item: res.data});
            this.mostrarDetalle();
         });
   }

   mostrarEstado(estado){
      if(estado === 1007)
         return "CERRADO";
      else
         return "ABIERTO";
   }

   toggle() {
      this.setState({modal: !this.state.modal});
	}

   mostrarDatosHistorico(id) {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/Inventarios/findDetalleById/${id}`)
         .then(res => {
            this.setState({historico: res.data, isLoading: false});
            this.mostrarDetalleHistorico(this.state.historico.inventarioId);
            this.toggle();
         });
   }

   cerrarInventario(){
      confirmAlert({
         title: 'Cerrar Inventario',
         message: 'Confirma cerrar el Inventario?',
         buttons: [
            {
               label: 'Si',
               onClick: () => {
                  axios.get(`${server.ip}/Inventarios/cerrarInventario/${this.state.item.inventarioId}/${JSON.parse(localStorage.getItem('user')).usuarioId}`)
                     .then(res => {
                        console.log(res);
                     })
                  //fetch(`/Inventarios/cerrarInventario/${this.state.item.inventarioId}/${JSON.parse(localStorage.getItem('user')).usuarioId}`);
                  this.mostrarDatos();
               }
            },
            {
               label: 'No',
               onClick: () => {}
            }
         ]
      })
      
   }

   mostrarDetalle(){
      const id = this.state.item.inventarioId;
      axios.get(`${server.ip}/DetalleInventario/findByInventarioId/${id}`)
         .then(res => {
            this.setState({detalles: res.data});
         });
   }

   mostrarDetalleHistorico(id){
      axios.get(`${server.ip}/DetalleInventario/findByInventarioId/${id}`)
         .then(res => {
            this.setState({detalleHistorico: res.data});
         });
   }

   imprimir(){
      var content = document.getElementById("printHistorico").innerHTML;
      var mywindow = window.open('', 'height=600,width=800');
      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('</head><body >');
      mywindow.document.write(content);
      mywindow.document.write('</body></html>');

      mywindow.document.close();
      mywindow.focus()
      mywindow.print();
      mywindow.close();
      return true;
   }

   render() {
      const {inventarios, isLoading, detalles, historico, detalleHistorico} = this.state;
      //console.log(historico);
      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const detalleList = detalles.map(det => {
         return <tr key={det.detalleInventarioId}>
            <td>{det.productoId.nombre}</td>
            <td>{det.cantidadDisponible}</td>
            <td>{det.cantidadUsada}</td>
            <td>{det.cantidadDisponible - det.cantidadUsada}</td>
         </tr>
      });

      const inventarioList = inventarios.map(inv => {
         return <tr key={inv.inventarioId}>
            <td>
               <Moment format="DD/MM/YYYY">
                  {inv.fechaInicio}
               </Moment>
            </td>
            <td>{this.mostrarEstado(inv.estadoInventarioId)}</td>
            <td>
               <Moment format="DD/MM/YYYY">
                  {inv.fechaCierre}
               </Moment>
            </td>
            <td>
               <Button color="success" onClick={() => this.mostrarDatosHistorico(inv.inventarioId)}> Ver Detalle</Button>
            </td>
         </tr>
      });
      const detalleHistoricoList = detalleHistorico.map(d => {
         return <tr key={d.detalleInventarioId}> 
            <td>{d.productoId.nombre}</td>
            <td>{d.cantidadDisponible}</td>
            <td>{d.cantidadUsada}</td>
            <td>{d.cantidadDisponible - d.cantidadUsada}</td> 
            </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <ModalHeader>Histórico</ModalHeader>
                  <ModalBody>
                  
                  <form className="form-inline">
                     <div id="printHistorico">
                        <div className="row">
                           <div className="form-group">
                              <label>Fecha Inicio: </label>
                              <Moment format="DD/MM/YYYY">
                                 {historico.fechaInicio}
                              </Moment>
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group">
                              <label>Fecha Cierre: </label>
                              <Moment format="DD/MM/YYYY">
                                 {historico.fechaCierre}
                              </Moment>
                           </div>
                        </div>
                        <Button className="default" onClick={this.imprimir}>Imprimir</Button>
                        <div className="row">
                           <div className="form-group col-md-12">
                              <Table className="mt-4">
                                 <thead>
                                    <tr>
                                       <th>Producto</th>
                                       <th>Cantidad Disponible</th>
                                       <th>Cantidad Usada</th>
                                       <th>Saldo</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {detalleHistoricoList}
                                 </tbody>
                              </Table>
                           </div>
                        </div>
                     </div>
                     
                     </form>
                  </ModalBody>
                  <ModalFooter>
                     <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                  </ModalFooter>
               </Modal>
               <h3>Inventarios</h3>
               <Tabs>
                  <TabList>
                     <Tab>Abierto</Tab>
                     <Tab>Histórico</Tab>
                  </TabList>
                  <TabPanel>
                     <Button color="success" onClick={this.cerrarInventario}>Cerrar Inventario</Button>
                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Producto</th>
                              <th>Cantidad Disponible</th>
                              <th>Cantidad Usada</th>
                              <th>Saldo</th>
                           </tr>
                        </thead>
                        <tbody>
                           {detalleList}
                        </tbody>
                     </Table>
                  </TabPanel>
                  <TabPanel>
                     <Table className="mt-4">
                        <thead>
                           <tr>
                              <th>Fecha Inicio</th>
                              <th>Estado</th>
                              <th>Fecha Cierre</th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>
                           {inventarioList}
                        </tbody>
                     </Table>
                  </TabPanel>
               </Tabs>
               
            </Container>
         </div>
      );
   }
}

export default InventarioList;