import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { server } from './servercontext';

class UsuarioList extends Component {
   emptyItem = {
      usuarioId: '',
      apellidos: '',
      nombres: '',
      username: '',
      password: '',
      habilitado: true
   };

   constructor(props) {
      super(props);
      this.state = {usuarios: [], isLoading: true, item: this.emptyItem};
      this.remove = this.remove.bind(this);

      // metodos modal
      this.toggle = this.toggle.bind(this);
      this.guardarDatos = this.guardarDatos.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.cambiarEstado = this.cambiarEstado.bind(this);
      this.mostrarEstado = this.mostrarEstado.bind(this);
      this.mostrarEstadoBoton = this.mostrarEstadoBoton.bind(this);
   }

   componentDidMount() {
      this.setState({isLoading: true});
      axios.get(`${server.ip}/usuario/findAll`)
         .then(res => {
            this.setState({usuarios: res.data, isLoading: false});
         });
      /*fetch('/usuario/findAll')
         .then(response => response.json())
         .then(data => this.setState({usuarios: data, isLoading: false}));*/
   }

   handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      let item = {...this.state.item};
      item[name] = value;
      this.setState({item});
   }

   async cambiarEstado(id, estado){
      await axios.get(`${server.ip}/usuario/cambiarEstado/${id}/${estado}`)
         .then(res => {
            let updatedUsuarios = [...this.state.usuarios]; //.filter(i => i.usuarioId !== id);
            let index = updatedUsuarios.findIndex(p => p.usuarioId === id);
            updatedUsuarios[index].habilitado = estado;
            this.setState({usuarios: updatedUsuarios});
         });
      /*await fetch(`/usuario/cambiarEstado/${id}/${estado}`, {
         method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
      }).then(() => {
         let updatedUsuarios = [...this.state.usuarios]; //.filter(i => i.usuarioId !== id);
         let index = updatedUsuarios.findIndex(p => p.usuarioId === id);
         updatedUsuarios[index].habilitado = estado;
         this.setState({usuarios: updatedUsuarios});
      });*/
   }

   mostrarEstado(estado){
      if(estado === true){
         return "Habilitado";
      } else {
         return "Deshabilitado";
      }
   }

   mostrarEstadoBoton(estado) {
      if(estado === true) {
         return "Deshabilitar";
      } else {
         return "Habilitar";
      }
   }

   toggle() {
      this.setState({modal: !this.state.modal});
   }

   async guardarDatos(event) {
      event.preventDefault();
      const {item} = this.state;

      await axios.post(`${server.ip}/usuario/insertOrUpdate`, item)
         .then(res => {
            this.setState({item: this.emptyItem});
            this.componentDidMount();
            this.toggle();
         });
      /*await fetch('/usuario/insertOrUpdate', {
         method: 'POST',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         },
         body: JSON.stringify(item),
      });*/
   }

   async remove(id) {
      await fetch(`${server.ip}/usuario/deleteById/${id}`)
         .then(res => {
            let updatedUsuarios = [...this.state.usuarios].filter(i => i.usuarioId !== id);
            this.setState({usuarios: updatedUsuarios});
         });
      /*await fetch(`/usuario/deleteById/${id}`, {
         method: 'GET',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
         }
      }).then(() => {
         let updatedUsuarios = [...this.state.usuarios].filter(i => i.usuarioId !== id);
         this.setState({usuarios: updatedUsuarios});
      });*/
   }

   nuevo() {
      this.toggle();
   }

   async editar(id) {
      await axios.get(`${server.ip}/usuario/findById/${id}`)
         .then(res => {
            this.setState({item: res.data});
            this.toggle();
         });
      /*const usuario = await(await fetch(`/usuario/findById/${id}`)).json();
      this.setState({item: usuario});
      this.toggle();*/
   }

   render() {
      const {usuarios, isLoading} = this.state;
      const {item} = this.state;
      const titulo = <h3>{item.usuarioId ? 'Editar Usuario' : 'Adicionar Usuario'}</h3>;

      if(isLoading) {
         return <p>Cargando...</p>;
      }

      const usuarioList = usuarios.map(usua => {
         return <tr key={usua.usuarioId}>
            <td>{usua.apellidos}</td>
            <td>{usua.nombres}</td>
            <td>{usua.username}</td>
            <td>{this.mostrarEstado(usua.habilitado)}</td>
            <td> 
               <ButtonGroup>
                  <Button size="sm" color="default" 
                     onClick={() => this.cambiarEstado(usua.usuarioId, !usua.habilitado)}>
                     {this.mostrarEstadoBoton(usua.habilitado)}
                  </Button>
                  <Button size="sm" color="primary" onClick={() => this.editar(usua.usuarioId)}>Editar</Button>
                  <Button color="success" tag={Link} to={"/Perfiles/" + usua.usuarioId}>Perfiles</Button>
               </ButtonGroup>
            </td>
         </tr>
      });

      return (
         <div>
            <AppNavbar/>
            <Container fluid>
               <Modal isOpen={this.state.modal}>
                  <form onSubmit={this.guardarDatos}>
                     <ModalHeader>{titulo}</ModalHeader>
                     <ModalBody>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Apellidos:</label>
                              <input type="text" name="apellidos" value={item.apellidos} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Nombres:</label>
                              <input type="text" name="nombres" value={item.nombres} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Login:</label>
                              <input type="text" name="username" value={item.username} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                        <div className="row">
                           <div className="form-group col-md-8">
                              <label>Password:</label>
                              <input type="password" name="password" value={item.password} className="form-control"
                                    onChange={this.handleChange} />
                           </div>
                        </div>
                     </ModalBody>
                     <ModalFooter>
                        <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                        <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                     </ModalFooter>
                  </form>
               </Modal>
               <h3>Listado de Usuarios</h3>
               <Button color="success" onClick={this.toggle}>Adicionar</Button>
               <Table>
                  <thead>
                     <tr>
                        <th>Apellidos</th>
                        <th>Nombres</th>
                        <th>Login</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                     </tr>
                  </thead>
                  <tbody>
                     {usuarioList}
                  </tbody>
               </Table>
            </Container>
         </div>
      );
   }
}

export default UsuarioList;