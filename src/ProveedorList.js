import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import AppNavbar from './AppNavbar';
import axios from 'axios';
import { server } from './servercontext';

class ProveedorList extends Component {
    emptyItem = {
        proveedorId: '',
        nombre: '',
        telefono1: '',
        telefono2: '',
        email: '',
        direccion: ''
    };

    constructor(props) {
        super(props);
        this.state = {proveedores: [], isLoading: true, item: this.emptyItem};
        this.remove = this.remove.bind(this);

        // metodos modal
        this.toggle = this.toggle.bind(this);
        this.guardarDatos = this.guardarDatos.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.setState({isLoading: true});
        axios.get(`${server.ip}/Proveedores/findAll`)
            .then(res => {
                this.setState({proveedores: res.data, isLoading: false});
            });
        /*fetch('/Proveedores/findAll')
            .then(response => response.json())
            .then(data => this.setState({proveedores: data, isLoading: false}));*/
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

    toggle() {
        this.setState({modal: !this.state.modal});
    }

    async guardarDatos(event) {
        event.preventDefault();
        const {item} = this.state;

        await axios.post(`${server.ip}/Proveedores/insertOrUpdate`, item)
            .then(res => {
                this.setState({item: this.emptyItem});
                this.componentDidMount();
                this.toggle();
            });
        /*await fetch('/Proveedores/insertOrUpdate', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        });*/
    }

    async remove(id) {
        //console.log("eliminando")
        await axios.get(`${server.ip}/Proveedores/delete/${id}`)
            .then(res => {
                let updatedProveedores = [...this.state.proveedores].filter(i => i.proveedorI !== id);
                this.setState({proveedores: updatedProveedores});    
            });
        /*await fetch(`/Proveedores/delete/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatedProveedores = [...this.state.proveedores].filter(i => i.proveedorI !== id);
            this.setState({proveedores: updatedProveedores});
        });*/
        //this.setState({item: this.emptyItem});
    }

    nuevo() {
        this.toggle();
    }

    async editar(id) {
        await axios.get(`${server.ip}/Proveedores/findById/${id}`)
            .then(res => {
                this.setState({item: res.data});
            });
        /*const proveedor = await(await fetch(`/Proveedores/findById/${id}`)).json();
        this.setState({item: proveedor});*/
        this.toggle();
    }

    render() {
        const {proveedores, isLoading} = this.state;
        const {item} = this.state;
        const titulo = <h3>{item.proveedorId ? 'Editar Proveedor' : 'Adicionar Proveedor'}</h3>

        if(isLoading) {
            return <p>Cargando...</p>;
        }

        const proveedorList = proveedores.map(prov => {
            return <tr key={prov.proveedorId}>
                <td>{prov.nombre}</td>
                <td>{prov.telefono1}</td>
                <td>{prov.telefono2}</td>
                <td>{prov.email}</td>
                <td>{prov.direccion}</td>
                <td> 
                    <ButtonGroup>
                    
                        <Button size="sm" color="primary" onClick={() => this.editar(prov.proveedorId)}>Editar</Button>
                        <Button size="sm" color="danger" onClick={(e) => { if (window.confirm('Confirma eliminar al Proveedor?')) this.remove(prov.proveedorId) } }>
                            Eliminar
                        </Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <Modal isOpen={this.state.modal}>
                        <form onSubmit={this.guardarDatos}>
                           <ModalHeader>{titulo}</ModalHeader>
                           <ModalBody>
                              <div className="row">
                                 <div className="form-group col-md-8">
                                    <label>Nombre:</label>
                                    <input type="text" name="nombre" value={item.nombre} className="form-control"
                                       onChange={this.handleChange} />
                                 </div>
                              </div>
                              <div className="row">
                                 <div className="form-group col-md-8">
                                    <label>Teléfono 1:</label>
                                    <input type="text" name="telefono1" value={item.telefono1} className="form-control"
                                       onChange={this.handleChange} />
                                 </div>
                              </div>
                              <div className="row">
                                 <div className="form-group col-md-8">
                                    <label>Teléfono 2</label>
                                    <input type="text" name="telefono2" value={item.telefono2} className="form-control"
                                       onChange={this.handleChange} />
                                 </div>
                              </div>
                              <div className="row">
                                 <div className="form-group col-md-8">
                                    <label>Correo Electrónico:</label>
                                    <input type="text" name="email" value={item.email} className="form-control"
                                       onChange={this.handleChange} />
                                 </div>
                              </div>
                              <div className="row">
                                 <div className="form-group col-md-8">
                                    <label>Dirección</label>
                                    <input type="text" name="direccion" value={item.direccion} className="form-control"
                                       onChange={this.handleChange} />
                                 </div>
                              </div>
                           </ModalBody>
                           <ModalFooter>
                              <input type="submit" value="Guardar" color="primary" className="btn btn-primary"/>
                              <Button color="danger" onClick={this.toggle}>Cancelar</Button>
                           </ModalFooter>
                        </form>
                    </Modal>
                    <h3>Listado de Proveedores</h3>
                    <Button color="success" onClick={this.toggle}>Adicionar</Button>
                    <Table className="mt-4">
                     <thead>
                        <tr>
                           <th>Nombre</th>
                           <th>Teléfono 1</th>
                           <th>Teléfono 2</th>
                           <th>E-mail</th>
                           <th>Dirección</th>
                           <th>Acciones</th>
                        </tr>
                     </thead>
                     <tbody>
                        {proveedorList}
                     </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ProveedorList;