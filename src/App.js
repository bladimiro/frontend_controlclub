import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ProductoList from './ProductoList';
import ProveedorList from './ProveedorList';
import UsuarioList from './UsuarioList';
import tipoGastoList from './tipoGastoList';
import SubproductoList from './SubproductoList';
import datosEmpresa from './datosEmpresa';
import salaList from './SalaList';
import MesaList from './MesaList';
import login from './login';
import pedidos from './Pedidos';
import pedidosPendientes from './PedidoList';
import clienteList from './ClienteList';
import GastoList from './GastoList';
import MovimientoList from './MovimientoList';
import IngresosEgresosList from './IngresosEgresosList';
import Home from './Home';
import PerfilesList from './PerfilesList';
import InventarioList from './InventarioList';
import ComprasList from './ComprasList';
import ChicasList from './ChicasList';
import pagosPendientes from './pedidosPendientes';
import cierreComandas from './cierreComandas';
import pedidoClienteVaso from './PedidoClienteVaso';
import pedidoChicaVaso from './PedidoChicaVaso';
import IngresosEgresos from './IngresosEgresos';
//
class App extends Component {
  render() {
    
    return (
      
      <Router>
        <Switch>
          <div>
            <Route path="/" exact={true} component={Home} />
            <Route path='/productos' exact={true} component={ProductoList} />
            <Route path='/proveedores' exact={true} component={ProveedorList} />
            <Route path='/usuarios' exact={true} component={UsuarioList} />
            <Route path='/tipogastos' exact={true} component={tipoGastoList} />
            <Route path="/subproductos/:id" exact={true} component={SubproductoList} />
            <Route path="/datosEmpresa" exact={true} component={datosEmpresa} />
            <Route path="/salas" exact={true} component={salaList} />
            <Route path="/mesas" exact={true} component={MesaList} />
            <Route path='/login' exact={true} component={login} />
            <Route path='/pedidos' exact={true} component={pedidos} />
            <Route path='/pedidosPendientes' exact={true} component={pedidosPendientes} />
            <Route path='/clientes' exact={true} component={clienteList} />
            <Route path='/gastos' exact={true} component={GastoList}/>
            <Route path='/movimientos' exact={true} component={MovimientoList} />
            <Route path='/resumenventas' exact={true} component={IngresosEgresosList} />
            <Route path='/perfiles/:id' exact={true} component={PerfilesList} />
            <Route path='/inventario' exact={true} component={InventarioList} />
            <Route path='/compras' exact={true} component={ComprasList} />
            <Route path='/chicas' exact={true} component={ChicasList} />
            <Route path='/pagosPendientes' exact={true} component={pagosPendientes} />
            <Route path='/cierreComandas' exact={true} component={cierreComandas} />
            <Route path='/pedidoClienteVaso' exact={true} component={pedidoClienteVaso} />
            <Route path='/pedidoChicaVaso' exact={true} component={pedidoChicaVaso} />
            <Route path='/ingresosEgresos' exact={true} component={IngresosEgresos} />
          </div>
          
        </Switch>
      </Router>
    );
  }
}

export default App;
